; Generate tilted microtubule reference images
; NB: assume all volume dimensions are the same!
; Oversample factor: if not 1, then the pixel size of the volume is that much smaller.
;  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Input parameters:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr 
?reference_base? <ref_base>
;
fr 
?input_volume? <vol>
;

rr x11
?voxel_size?
;
rr x12
?helical_repeat_dist?
;
rr x13, x14
?bin_factor oversample_factor?
;
rr x15
?filter_resolution?
;
rr x16
?final_box_dim?
;
                                       ;  so they can be applied to larger
                                       ;  data images
rr x17, x18, x19, x20, x21
?min_phi max_phi min_theta max_theta angle_step?
;
rr x22
?psi?
;
rr x23, x24
?psi?
;

;;;;;;;;;;;;
; A final, combining step is specified by $which_split=0
;;;;;;;;;;;;
  if(x23 .EQ. 0) then
iq fi x25
<ref_base>/ref_tot
;
    if(x25 .EQ. 1) then
vm 
/bin/rm [ref_base]/ref_tot.spi
;
    endif

    x26 = 1

vm 
echo Merging {***x24} files:
;
    do LB5 x27 = 1, x24
fi x x28
<ref_base>/ref_tot_split{***x27}@
(26)
;
vm 
printf ' '{***x27}
;
      do LB6 x29 = 1, x28
cp 
<ref_base>/ref_tot_split{***x27}@{*******x29}
<ref_base>/ref_tot@{*******x26}
;
           x26 = x26 + 1
      LB6
vm 
/bin/rm [ref_base]/ref_tot_split{***x27}.spi
;
;      vm(/bin/rm <ref_base>/ref_angle_temp{***$i}.spi)
;      vm(/bin/rm <ref_base>/ref_angle_split{***$i}.spi)
    LB5

;;;;;;;;;;;;
; Quit if finished with the combining step
;;;;;;;;;;;;
    en de

  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fi x30, x31, x32
<vol>
(12, 2, 1)
;

x33 = 0
if(x30 .NE. x31) x33 = 1
if(x30 .NE. x32) x33 = 1
if(x33.EQ.1) then
;  vm(echo 'ERROR: reference volume dimensions {****$vol_x} {****$vol_y} {****$vol_z} must be equal...')
;  en de
endif

if(x14 .GT. x13) then
  x14 = x13
endif
x34 = x13/x14


if(x34 .NE. 1) then
@downsample_centered
<vol>
_1
(x34)
;
else
cp 
<vol>
_1
;
vm 
echo Volume not downsampled
;
endif

x35 = x11*x34
x36 = 20/(x35)

if(x15 .GT. 0) then
  x37 = 1
  x38 = x15 + x37
  x39 = x15 - x37
  x40 = 0.5*(2*x35)/x38
  x41 = 0.5*(2*x35)/x39

  x42 = 7
fq 
_1
_2
(x42)
(x40, x41)
;
else
cp 
_1
_2
;
vm 
echo Volume not filtered
;
endif

x43 = x35*x14

fi x30, x31, x32
_2
(12, 2, 1)
;
x44 = x30

; Pad volume to make sure the binned volume has even, integral dimensions
;  NOTE EVIL SPIDER BUG: 'pj 3q' has the WRONG ORIGIN if the volume dimensions
;   are odd!

x45 = 2*x14*int( (x32+2*x14 - 1) / (2*x14) )
x46 = 1 + int(x45/2) - int(x30/2)
x47 = 1 + int(x45/2) - int(x31/2)
x48 = 1 + int(x45/2) - int(x32/2)
pd 
_2
_1
(x45, x45, x45)
N
(0)
(x46, x47, x48)
;

x44 = x45

x49 = x12/x43

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

x50 = x22          ; Could make this more general, just handle filaments for now, and do only
x51 = x22          ;   one polarity (because other polarity is related by symmetry)

; $max_i = int(($max_psi-$min_psi)/$angle_step) + 1

if(x51.eq.270) then
  x52 = 2
else
  x52 = 1
endif

x53 = int((x20-x19)/x21) + 1
x54 = int((x18-x17)/x21) + 1

x26 = 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Generate document file for projection command "pj 3q"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

x55 = (x20-x19+1)/x21
if(x55 .LT. 1) then
  x55 = 1
endif
x55 = x55 * 360

sd ic new 
ref_angle
(5, x55)
;

if(x24 .NE. 1) then
sd ic new 
ref_angle_split
(5, x55)
;
endif

do LB1 x27 = 1, x52 
  x22 = x50 + 180*(x27-1)

  do LB2 x29 = 1, x53
    x56 = x19 + x21*(x29-1)

    do LB3 x57 = 1, x54
      x26 = x26 + 1
      x58 = x17 + x21*(x57-1)

sd ic x26, x22, x56, x58
ref_angle
;
    LB3
  LB2
LB1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Save reference projection parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if(x23 .EQ. 1) then
de 
<ref_base>/ref_angle
;
sd ic copy 
ref_angle
<ref_base>/ref_angle
;

sd 1, x17, x18, x19, x20, x21
<ref_base>/ref_params
;
sd 2, x12, x49, x43, x59
<ref_base>/ref_params
;
sd 3, x52, x53, x54
<ref_base>/ref_params
;
endif

x60 = x26

if(x24 .EQ. 1) then
fr l 
<stack_name> <ref_base>/ref_tot
;
fr l 
<angle_doc> ref_angle
;

  x61 = 1
  x62 = x60
else
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Split up the tasks if requested
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr l 
<angle_doc> ref_angle_split
;
fr l 
<stack_name> <ref_base>/ref_tot_split{***x23}
;

  x28 = x60/x24

  if(x28 .NE. int(x28)) then
    x28 = int(x28) + 1
  endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make a mini angle doc file for each split up job:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  x61 = 1 + (x23 - 1)*x28
  x62 = x23*x28

  if(x62 .GT. x60) then
    x62 = x60
  endif

  x63 = x62 - x61 + 1

  x26 = x61
  do LB7 x27 = 1,x63
ud ic x26, x22, x56, x58
ref_angle
;
sd ic x27, x22, x56, x58
ref_angle_split
;
     x26 = x26 + 1
  LB7

endif

x63 = x62 - x61 + 1

;;;;;;;;;;;;
; Define the final image size
;;;;;;;;;;;;

if(x16 .EQ. 0) then
  x64 = x44
else
  x64 = x16
endif

x64 = int(x64/x14/2) * 2*x14

vm 
echo Projecting {******x63} images...
;

ms 
_10@
(x44, x44)
(x63)
;

;;;;;;;;;;;;
; Dump one image just so we can see what it looks like
;;;;;;;;;;;;

x65 = x64
x66 = 1 + int(x44/2) - int(x65/2)
x67 = x64
x68 = 1 + int(x67/2) - int(x44/2)

if(x23 .EQ. 1) then
pj 3q 
_1
(x44)
1-1
<angle_doc>
_10@******
;
  if(x64 .LE. x44) then
wi 
_10@1
_2
(x65, x65)
(x66, x66)
;
    x69 = x64/2 - 1 - x36
  else
pd 
_10@1
_2
(x67, x67)
N
(0)
(x68, x68)
;
    x69 = x44/2 - 1 - x36
  endif
@downsample_centered
_10@1
<ref_base>/ref_tot@1
(x14)
;
endif

;;;;;;;;;;;;
; Now make the projections
;;;;;;;;;;;;

pj 3q 
_1
(x44)
1-x63
<angle_doc>
_10@******
;
  
; downsample to avoid aliasing artifacts

iq fi x25
<stack_name>
;
if(x25 .EQ. 1) then
vm 
/bin/rm [stack_name].spi
;
endif

;;;;;;;;;;;;
; Bin and resize the projections
;;;;;;;;;;;;

do LB4 x27 = 1, x63

  if(x64 .LE. x44) then
wi 
_10@{******x27}
_2
(x65, x65)
(x66, x66)
;
    x69 = x64/2 - 1 - x36
  else
pd 
_10@{******x27}
_2
(x67, x67)
N
(0)
(x68, x68)
;
    x69 = x44/2 - 1 - x36
  endif

  x70 = 1 + int(x64/2)
;  ma(_2, _3, $mask_radius 0, C, E, 0, $ma_oxy $ma_oxy, $mask_edge_width)
;  cp(_3, _2)

@downsample_centered
_2
_1
(x14)
;

cp 
_1
<stack_name>@{******x27}
;
LB4

; (1, $final_dim, $vol_dim, $mask_edge_width, $mask_radius) = sd(debug_doc)

en de

; Warning: the following variables were used only once: $micrograph_pixel_size $ma_oxy 
