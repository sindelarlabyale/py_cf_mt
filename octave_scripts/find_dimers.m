function id_map = find_dimers(sorted_map,seam_mask, sorted_map_file, singledimer)

if(nargin < 4)
  singledimer = 0;
end

% sorted_map = readSPIDERfile(sorted_map_file);
s = size(sorted_map);

result_maps = zeros(s(1),s(2),2);
id_map = zeros(s(1),s(2));

for pf = 1:s(1)
  if(seam_mask(pf,1))
    register = 1;
  else
    register = 2;
  end

for row = register+2:2:s(2)-2

  if(sorted_map(pf,row) && !sorted_map(pf,row-2))
    for ind=row+2:2:s(2)-2
%      fprintf('%2d %3d %3d %1d %d\n',pf,row,ind,sorted_map(pf,row),sorted_map(pf,ind));
      if(!sorted_map(pf,ind))
        break
      end
    end
    n_kinesins = (ind-row)/2;
%    fprintf('%2d %d\n',pf,n_kinesins);
    if(mod(n_kinesins,2) == 0)
      result_index = 1;
    else
      result_index = 2;
    end

    if(ind == s(2)-2)
      result_index = 2;
    end

    if(singledimer)
      if(n_kinesins != 2)
        result_index = 2;
      end
    end

    result_maps(pf,row,result_index) = 1;
    id = 0;
    if(result_index == 1)
      id_map(pf,row) = id+1;
    end

    for ind=row+2:2:s(2)
      id = mod((id + 1),2);
      if(!sorted_map(pf,ind))
        break
      else
        result_maps(pf,ind,result_index) = 1;
        if(result_index == 1)
          id_map(pf,ind) = id+1;
        end
      end
    end
  end
end
end

if(nargin > 2)
  if(!isempty(sorted_map_file))
    output_file = sprintf('%s_good.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
    writeSPIDERfile(output_file,result_maps(:,:,1));
    output_file = sprintf('%s_bad.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
    writeSPIDERfile(output_file,result_maps(:,:,2));

    output_file = sprintf('%s_good_id.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
    writeSPIDERfile(output_file,id_map);
  end
end

fprintf('%4d %4d %6.4f\n',sum(sum(result_maps(:,:,1))),sum(sum(result_maps(:,:,2))),sum(sum(result_maps(:,:,1)))/sum(sum(result_maps(:,:,2))));
