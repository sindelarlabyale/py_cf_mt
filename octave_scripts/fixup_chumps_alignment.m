function [coords_est,phi_est,theta_est,psi_est,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat, num_pfs, num_starts, ref_com)

n_good_boxes = prod(size(phi));
%%%%%%%%%%%%%%%%%%%%%%%%
% If the user has selected the filament with reversed polarity, 
%  reverse the order of coordinates. Otherwise we would need to
%  turn psi around (add 180)
%%%%%%%%%%%%%%%%%%%%%%%5

if(directional_psi != 270)
%if(directional_psi == -1)
  tempcoords = coords(:,:);
  tempphi = phi(:);
  temptheta = theta(:);
  temppsi = mod(psi(:)+0, 360);

  for j=1:n_good_boxes
    coords(j,:) = tempcoords(n_good_boxes - j + 1, :);
    phi(j,:) = tempphi(n_good_boxes - j + 1);
    theta(j,:) = temptheta(n_good_boxes - j + 1);
    psi(j,:) = temppsi(n_good_boxes - j + 1);
  end
  directional_psi = 270;
end

smoothing_hwidth = 5; % Halfwidth for smoothing euler angles

%%%%%%%%%%%%%%%%%%%%%
% Regularize the spacing between repeats, shifting each one to
%  make the consecutive distances ~80A; 
%  also, estimate psi from the particle coordinates
%%%%%%%%%%%%%%%%%%%%%

phi_est = zeros([1 n_good_boxes]);
theta_est = zeros([1 n_good_boxes]);
psi_est = zeros([1 n_good_boxes]);
d_phi_d_repeat = zeros([1 n_good_boxes]);
d_theta_d_repeat = zeros([1 n_good_boxes]);
d_psi_d_repeat = zeros([1 n_good_boxes]);

dist_from_last = zeros([1 n_good_boxes]);

for j=2:n_good_boxes
  dist_from_last(j) = norm(coords(j,:) - coords(j-1,:));
end

j=2;
while(j <= n_good_boxes)
  vec_from_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_from_last(2),vec_from_last(1));
  dist_from_last(j) = norm(vec_from_last);

  tub_monomer_spacing = round(2*dist_from_last/est_pix_per_repeat);

%%%%%%%%%%%%%%%
% Determine whether the current box is displaced forward or backwards from the
%  next expected position.
% Note that if directional_psi is 90 (rather than 270), 
%  then the direction indicated by psi points backwards, and we need to account for that.
%%%%%%%%%%%%%%%

  if( (cos(-pi/180*psi(j))*vec_from_last(1) + sin(-pi/180*psi(j))*vec_from_last(2)) > 0)
    tub_monomer_spacing = -tub_monomer_spacing;
  end
  if(directional_psi == 90)
    tub_monomer_spacing = -tub_monomer_spacing;
  end    
%%%%%%%%%%%%%%%%%%%%
% Adjust the current repeat coordinates by n monomer repeats forwards or backward
%%%%%%%%%%%%%%%%%%%%

  if(tub_monomer_spacing(j) >= 2)
    coords(j,:) = coords(j,:) - ...
      (coords(j,:)-coords(j-1,:))*(tub_monomer_spacing(j)-2)/tub_monomer_spacing(j);
  else
%%%%%%%%%%%%%%%%%%%%
% If the current box coordinates have fallen behind the expected position,
% get rid of the current box and shift all information
%  to the left.
%%%%%%%%%%%%%%%%%%%%

    n_good_boxes = n_good_boxes - 1;

    if(j < n_good_boxes)
      coords(j:n_good_boxes,:) = coords(j+1:n_good_boxes+1,:);
      phi(j:n_good_boxes) = phi(j+1:n_good_boxes+1);
      theta(j:n_good_boxes) = theta(j+1:n_good_boxes+1);;
      psi(j:n_good_boxes) = psi(j+1:n_good_boxes+1);
      psi_est(j:n_good_boxes) = psi_est(j+1:n_good_boxes+1);
    end
    coords = coords(1:n_good_boxes,:);
    phi = phi(1:n_good_boxes);
    theta = theta(1:n_good_boxes);
    psi = psi(1:n_good_boxes);
    psi_est = psi_est(1:n_good_boxes);
    j=j-1;
  end

  if(j > 1)
    dist_from_last(j) = norm(coords(j,:) - coords(j-1,:));
  end

  j = j + 1;
end

%%%%%%%%%%%%%%%%%%%%%
% Smooth the Euler angles
%%%%%%%%%%%%%%%%%%%%%

dist_from_last = zeros([1 n_good_boxes]);

for j=2:n_good_boxes
  vec_from_last = coords(j,:) - coords(j-1,:);
  psi_est(j) = -180/pi*atan2(vec_from_last(2),vec_from_last(1));
  dist_from_last(j) = norm(vec_from_last);
end

pix_per_repeat_from_params = helical_repeat_distance/micrograph_pixel_size
pix_per_repeat_from_coords = sum(dist_from_last(2:prod(size(dist_from_last))))/(prod(size(dist_from_last))-1)

psi_est(1) = psi_est(2);

%%%%%%%%
% We use the following arrays to avoid wraparound issues near 0 and 360
%%%%%%%%
phi_temp_offset = zeros([1 n_good_boxes]);
psi_temp_offset = zeros([1 n_good_boxes]);
phi_temp_offset(find(phi < 90)) = 180;
phi_temp_offset(find(phi > 270)) = 180;
psi_temp_offset(find(psi < 90)) = 180;
psi_temp_offset(find(psi > 270)) = 180;

for j=1:n_good_boxes

  time1=cputime();
  cur_phi_est = 0;
  cur_theta_est = 0;
  cur_psi_est = 0;
  count = 0;

  l_bound = j-smoothing_hwidth-1;
  if(l_bound < 1)
    l_bound = 1;
  end
  u_bound = j+smoothing_hwidth;
  if(u_bound > n_good_boxes)
    u_bound = n_good_boxes;
  end

  phi_temp = mod(phi(l_bound:u_bound) + phi_temp_offset(j),360);
  theta_temp = theta(l_bound:u_bound);
  psi_temp = mod(psi(l_bound:u_bound) + psi_temp_offset(j),360);

%%%%%%%%%%%%%%%
% Discard outliers and do linear fits, if possible
%%%%%%%%%%%%%%%

  phi_avg = sum(phi_temp(:))/prod(size(phi_temp));
  phi_std = sqrt(var(phi_temp(:)));
  theta_avg = sum(theta_temp(:))/prod(size(theta_temp));
  theta_std = sqrt(var(theta_temp(:)));
  psi_avg = sum(psi_temp(:))/prod(size(psi_temp));
  psi_std = sqrt(var(psi_temp(:)));

  j_temp = l_bound:u_bound;
  j_temp = j_temp(find(abs(phi_temp - phi_avg) < 2*phi_std));
  phi_temp = phi_temp(find(abs(phi_temp - phi_avg) < 2*phi_std));
  if(prod(size(phi_temp > 1)))
%    phi_params=polyfit(j_temp,phi_temp,1);
    phi_params=linear_fit(j_temp,phi_temp,1);

    phi_est(j) = mod(phi_params(1)*j+phi_params(2)-phi_temp_offset(j),360);
    d_phi_d_repeat(j) = phi_params(1);

  else
    phi_est(j) = phi(j);
    d_phi_d_repeat(j) = 0;
  end

  j_temp = l_bound:u_bound;
  j_temp = j_temp(find(abs(theta_temp - theta_avg) < 2*theta_std));
  theta_temp = theta_temp(find(abs(theta_temp - theta_avg) < 2*theta_std));

  if(prod(size(theta_temp > 1)))
%    theta_params=polyfit(j_temp,theta_temp,1)
    theta_params=linear_fit(j_temp,theta_temp);
    theta_est(j) = theta_params(1)*j+theta_params(2);
    d_theta_d_repeat(j) = theta_params(1);
  else
    theta_est(j) = theta(j);
    d_theta_d_repeat(j) = 0;
  end

  j_temp = l_bound:u_bound;
  j_temp = j_temp(find(abs(psi_temp - psi_avg) < 2*psi_std));
  psi_temp = psi_temp(find(abs(psi_temp - psi_avg) < 2*psi_std));
  if(prod(size(psi_temp > 1)))
%    psi_params=polyfit(j_temp,psi_temp,1);
    psi_params=linear_fit(j_temp,psi_temp);
    psi_est(j) = mod(psi_params(1)*j+psi_params(2)-psi_temp_offset(j),360);
    d_psi_d_repeat(j) = psi_params(1);
  else
    psi_est(j) = psi(j);
    d_psi_d_repeat(j) = 0;
  endif
end

est_repeat_distance = pix_per_repeat_from_coords*micrograph_pixel_size
est_repeat_distance = est_repeat_distance / (sum(sin(theta(:)*pi/180))/prod(size(theta)))
est_pix_per_repeat

%%%%%%%%%%%%%%%
% Convert derivatives to units of distance
%%%%%%%%%%%%%%%

d_phi_d_repeat = d_phi_d_repeat/est_repeat_distance;
d_theta_d_repeat = d_theta_d_repeat/est_repeat_distance;
d_psi_d_repeat = d_psi_d_repeat/est_repeat_distance;

%%%%%%%%%%%%%%%
% If directional_psi is 90 (rather than 270), 
%  the MT segment points backwards relative to the 
%  path of the boxes...
%%%%%%%%%%%%%%%

if(directional_psi == 90)
  d_phi_d_repeat = -d_phi_d_repeat;
  d_theta_d_repeat = -d_theta_d_repeat;
  d_psi_d_repeat = -d_psi_d_repeat;
%  psi_est = psi_est + 180;
end

%%%%%%%%%%%%%%%%%%
% We adjust d_phi_d_repeat so it reflects the change in phi twist *relative* to 
%  the expected change in phi twist per subunit, for a given microtubule type.
%%%%%%%%%%%%%%%%%%
mt_radius13pf = sqrt(ref_com(1)^2 + ref_com(2)^2);
[twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat] = ...
  mt_lattice_params(num_pfs, num_starts, est_repeat_distance, mt_radius13pf);

d_phi_d_repeat = d_phi_d_repeat - twist_per_repeat * 180/pi / est_repeat_distance;

coords_est = coords;
