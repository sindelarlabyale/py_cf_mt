function [val derivative] = modulate_parameter(x, params)
% function [val derivative] = modulate_parameter(x, params)
% 
% Generate a sin function/linear function
%  scalar x; 1D array params = [a] or [a b] or [a b c d]
% 
% This function has two modes:
% 1. linear mode, if size(params) is not 4 or if parameter c is zero
%   val = a + b*x
%
% Note: for this mode, "a" should be in units of the output "val";
%                      "b" should be (units of "val") / (units of "x")
%   
% 2. oscillating mode, if parameter c != 0:
%   val = a + b*sin(2*pi*(x+d)/c);
%
%   a = value at x=0 (units of "val")
%   b = amplitude of oscillation (units of "val")
%   c = period of oscillation (in same units as x)
%   d = x offset of the zero value (in same units as x)
%
%%%%%%%%%%%%%%%%%%%%

a = params(1);
if(size(params(:)) == 1)
  b = 0;
else
  b = params(2);
end

if(size(params) < 4)
  c = 0;
  d = 0;
else
  c = params(3);
  d = params(4);
end

if(c == 0)
  val = a + b*x;
  derivative = b*ones(1,prod(size(x)));
else
  val = a + b*sin(2*pi*(x+d)/c);
  derivative = b*cos(2*pi*(x+d)/c)*2*pi/c;
end
