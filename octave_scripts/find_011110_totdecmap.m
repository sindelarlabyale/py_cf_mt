function id_map = find_011110_totdecmap(tot_decoration_map_hd,tot_decoration_map_ld, seam_mask)

% seam_mask = find_seams_totdecmap(tot_decoration_map_hd);
% sorted_map = readSPIDERfile(sorted_map_file);
s = size(tot_decoration_map_hd);

id_map = zeros(s(1),s(2));

for pf = 1:s(1)
  if(!seam_mask(pf,1))
    continue
  end

  for row = 2 : s(2)-4  
    if(tot_decoration_map_hd(pf,row) && !tot_decoration_map_hd(pf,row-1) && tot_decoration_map_hd(pf,row+1) && tot_decoration_map_hd(pf,row+2) && tot_decoration_map_hd(pf,row+3) && !tot_decoration_map_hd(pf,row+4) && !tot_decoration_map_ld(pf,row-1) && !tot_decoration_map_ld(pf,row+4))
      id_map(pf,row) = 1;
      row = row+1;
      id_map(pf,row) = 2;
      row = row+1;
      id_map(pf,row) = 3;
      row = row+1;
      id_map(pf,row) = 4;
    end
  end
end
