function output_image = smear_filament_background(box_image, circular_mask_radius)
% function smear_filament_background(stack, circular_mask_radius)
%
%  For images of horizontal filament segments, to which a circular mask has already been applied:
%   Adjusts the background to minimizing artifacts that would arize from filtering.
% 

mask_falloff_pixels = 4;
m = size(box_image);

if(nargin < 2)
  circular_mask_radius = 0.45 * m(1);
end

[x,y]=ndgrid(-m(1)/2:m(1)/2-1, -m(2)/2:m(2)/2-1);
r=sqrt(x.^2+y.^2);

% Pick a value near the edge of the mask for filling in the background
val = box_image(m(1)/2, floor(m(2)/2 + circular_mask_radius) - 1);
box_image(find(r > circular_mask_radius + ceil(mask_falloff_pixels/2))) = val;

circ_mask = fuzzymask(m(1),2,circular_mask_radius,mask_falloff_pixels);

% Fill in values outside the mask with horizontally smeared background values

% First, get a vertical strip of representative background values
vals_strip = box_image(m(1)/2, :);

% Then, low-pass filter the strip to make it smooth
vals_strip = ifftn(fftn(vals_strip) .* fftshift(fuzzymask(m(1),1,0.1*m(1),4))');

% Then, smear the strip into a 2D image and paste it into the background
output_image = box_image .* circ_mask + repmat(vals_strip, [m(1) 1]) .* (1-circ_mask);

