function map = pad_gently(input_map, wi_total_dim, wi_orig, wi_total_orig, mirrorpad, noise_box);

%%%%%%%%%%%
% Fill in any part of a box that falls outside the volume it was windowed from,
%  using either a 'noise box' and/or
%  a mirror image of the matching edge segment.
%
% wi_total_dim is final desired size; wi_total_orig is the intended origin of the
%  box/volume with respect to the larger volume it was windowed from, which may
%  not lie within the actual larger volume.
%
% wi_orig is the clipped origin of the box
%  with respect to the larger volume it was windowed from (will always lie within larger volume)
%%%%%%%%%%%

if(nargin < 5)
  mirrorpad = 0;
end

add_noise_box = 0;
if(nargin >= 6)
  if(!isempty(noise_box))
    add_noise_box = 1;
  end
end

if(nargin < 7)
  do_fade = 1;
end

wi_dim = size(input_map);
if(prod(size(wi_dim)) < 2)
  wi_dim = [wi_dim 1];
end
if(prod(size(wi_dim)) < 3)
  wi_dim = [wi_dim 1];
end

if(prod(size(wi_orig)) < 2)
  wi_orig = [wi_orig 1];
end
if(prod(size(wi_orig)) < 3)
  wi_orig = [wi_orig 1];
end

if(prod(size(wi_total_dim)) < 2)
  wi_total_dim = [wi_total_dim 1];
end
if(prod(size(wi_total_dim)) < 3)
  wi_total_dim = [wi_total_dim 1];
end

if(prod(size(wi_total_orig)) < 2)
  wi_total_orig = [wi_total_orig 1];
end
if(prod(size(wi_total_orig)) < 3)
  wi_total_orig = [wi_total_orig 1];
end

if(!isequal(wi_dim, wi_total_dim))
  if(add_noise_box)
    background = noise_box;
  elseif(!isempty(input_map))
    background = sum(sum(sum(input_map)))/prod(size(input_map));
  else
    background = 0;
  end

  map = ones(wi_total_dim).*background;
else
  add_noise_box = 0;
end

if(! isempty(input_map))
  map(1 + wi_orig(1) - wi_total_orig(1):1 + wi_orig(1) - wi_total_orig(1) + wi_dim(1)-1,
      1 + wi_orig(2) - wi_total_orig(2):1 + wi_orig(2) - wi_total_orig(2) + wi_dim(2)-1,
      1 + wi_orig(3) - wi_total_orig(3):1 + wi_orig(3) - wi_total_orig(3) + wi_dim(3)-1) = ...
     input_map;
end

%%%%%%%%%%%%%%%%%%%%%%%
% Fancy and probably unnecessary code for mirror image padding
%%%%%%%%%%%%%%%%%%%%%%%

if(mirrorpad && !isempty(input_map))

for dim=1:3
  if(wi_dim(dim) < wi_total_dim(dim))
%%%%%%%%%%%%%%%%%%%%%%
% If the windowed box was cut off on the left side
%%%%%%%%%%%%%%%%%%%%%%
    stripwidth = 0;
    extra_stripwidth = 0;
    if(wi_total_orig(dim) < 1)
      stripwidth = 1 - wi_total_orig(dim);

%%%%%%%%%%%%%%%%%%%%%%
% extrastrip defines the area that can't be filled in by mirror images
%%%%%%%%%%%%%%%%%%%%%%
      if(stripwidth > wi_dim(dim) - 1)
        extra_stripwidth = stripwidth - (wi_dim(dim) - 1);
        stripwidth = wi_dim(dim) - 1;
      else
        extra_stripwidth = 0;
      end

      background_val = 0;
      if(extra_stripwidth != 0)
        if(!do_fade)
          fade = 0;
        else
          fade = 1 - [0:stripwidth-1]/(stripwidth-1);
          if(dim == 1)
            fade = repmat(fade(:), [1 wi_dim(2) wi_dim(3)]);
            background_val = input_map(1,:,:);
          elseif(dim == 2)
            fade = repmat(fade(:)', [wi_dim(1) 1 wi_dim(3)]);
            background_val = input_map(:,1,:);
          elseif(dim == 3)
            temp = zeros([1 1 stripwidth]);
            temp(1,1,:) = fade(:);
            fade = repmat(temp, [wi_dim(1) wi_dim(2) 1]);
            background_val = input_map(:,:,1);
          end
          background_val = median(background_val(:));
        end
      else
        fade = 0;
      end

      if(dim == 1)
        if(add_noise_box == 0)
          map(1:stripwidth+extra_stripwidth,1:wi_dim(2),1:wi_dim(3)) = ...
            background_val;
        end

        map(1+extra_stripwidth:stripwidth+extra_stripwidth,1:wi_dim(2),1:wi_dim(3)) = ...
          map(1+extra_stripwidth:stripwidth+extra_stripwidth,...
              1:wi_dim(2),1:wi_dim(3)) .* fade + ...
          map(stripwidth+extra_stripwidth+stripwidth+1:-1:stripwidth+extra_stripwidth+1+1,...
              1:wi_dim(2),1:wi_dim(3)) .* (1-fade);

      elseif(dim == 2)
        if(add_noise_box == 0)
          map(1:wi_dim(1),1:stripwidth+extra_stripwidth,1:wi_dim(3)) = ...
            background_val;
        end

        map(1:wi_dim(1),...
            1+extra_stripwidth:stripwidth+extra_stripwidth,1:wi_dim(3)) = ...
          map(1:wi_dim(1),...
              1+extra_stripwidth:stripwidth+extra_stripwidth,1:wi_dim(3)) .* fade + ...
          map(1:wi_dim(1),...
              stripwidth+extra_stripwidth+stripwidth+1:-1:stripwidth+extra_stripwidth+1+1,...
              1:wi_dim(3)) .* (1-fade);

      elseif(dim == 3)
        if(add_noise_box == 0)
          map(1:wi_dim(1),1:wi_dim(2),1:stripwidth+extra_stripwidth) = ...
            background_val;
        end

        map(1:wi_dim(1),1:wi_dim(2),...
            1+extra_stripwidth:stripwidth+extra_stripwidth) = ...
        map(1:wi_dim(1),1:wi_dim(2),...
            1+extra_stripwidth:stripwidth+extra_stripwidth) .* fade + ...
          map(1:wi_dim(1),1:wi_dim(2),...
              stripwidth+extra_stripwidth+stripwidth+1:-1:stripwidth+extra_stripwidth+1+1) .* ...
              (1-fade);
      end
    end

%%%%%%%%%%%%%%%%%%%%%%
% If the windowed box was cut off on the right side
%%%%%%%%%%%%%%%%%%%%%%

    lstripwidth = extra_stripwidth + stripwidth;

    stripwidth = 0;
    extra_stripwidth = 0;

    if(wi_total_orig(dim) + wi_total_dim(dim) - 1 > wi_dim(dim))
      stripwidth = wi_total_dim(dim) - wi_dim(dim) - lstripwidth;

      if(stripwidth > wi_dim(dim) - 1)
        extra_stripwidth = stripwidth - (wi_dim(dim) - 1);
        stripwidth = wi_dim(dim) - 1;
      else
        extra_stripwidth = 0;
      end

      background_val = 0;
      if(extra_stripwidth != 0)
        if(!do_fade)
          fade = 0;
        else
          fade = 1 - [stripwidth-1:-1:0]/(stripwidth-1);
          if(dim == 1)
            fade = repmat(fade(:), [1 wi_dim(2) wi_dim(3)]);
            background_val = input_map(wi_dim(1),:,:);
          elseif(dim == 2)
            fade = repmat(fade(:)', [wi_dim(1) 1 wi_dim(3)]);
            background_val = input_map(:,wi_dim(2),:);
          elseif(dim == 3)
            temp = zeros([1 1 stripwidth]);
            temp(1,1,:) = fade(:);
            fade = repmat(temp, [wi_dim(1) wi_dim(2) 1]);
            background_val = input_map(:,:,wi_dim(3));
          end
          background_val = median(background_val(:));
        end
      else
        fade = 0;
      end

      if(dim == 1)
        if(add_noise_box == 0)
          map(wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim),1:wi_dim(2),1:wi_dim(3)) = ...
            background_val;
        end

        map(wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
            wi_total_dim(dim) - extra_stripwidth,1:wi_dim(2),1:wi_dim(3)) = ...
          map(wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim) - extra_stripwidth,1:wi_dim(2),1:wi_dim(3)) .*fade + ...
          map(wi_total_dim(dim) - stripwidth - extra_stripwidth - 1:-1:...
              wi_total_dim(dim) - stripwidth - extra_stripwidth - stripwidth + 1 - 1,...
              1:wi_dim(2),1:wi_dim(3)) .* (1-fade);

      elseif(dim == 2)
        if(add_noise_box == 0)
          map(1:wi_dim(1), wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim),1:wi_dim(3)) = ...
            background_val;
        end

        map(1:wi_dim(1),...
            wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
            wi_total_dim(dim) - extra_stripwidth,1:wi_dim(3)) = ...
          map(1:wi_dim(1),...
              wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim) - extra_stripwidth,1:wi_dim(3)) .* fade + ...
          map(1:wi_dim(1),...
              wi_total_dim(dim) - stripwidth - extra_stripwidth - 1:-1:...
              wi_total_dim(dim) - stripwidth - extra_stripwidth - stripwidth + 1 - 1,...
              1:wi_dim(3)) .* (1-fade);

      elseif(dim == 3)
        if(add_noise_box == 0)
          map(1:wi_dim(1),1:wi_dim(2),wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim)) = ...
            background_val;
        end

        map(1:wi_dim(1),1:wi_dim(2),...
            wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
            wi_total_dim(dim) - extra_stripwidth) = ...
          map(1:wi_dim(1),1:wi_dim(2),...
              wi_total_dim(dim) - stripwidth - extra_stripwidth + 1:...
              wi_total_dim(dim) - extra_stripwidth) .* fade + ...
          map(1:wi_dim(1),1:wi_dim(2),...
              wi_total_dim(dim) - stripwidth - extra_stripwidth - 1:-1:...
              wi_total_dim(dim) - stripwidth - extra_stripwidth - stripwidth + 1 - 1) .* (1-fade);
      end
    end
%    wi_dim(dim) = wi_total_dim(dim);
  end
end
end
