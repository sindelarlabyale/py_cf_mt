function [guessed_coords, coordinate_distributions] = ...
  mt_squashed_coords_from_phi(patch_phis, patch_xs, grid_xy, grid_dxy,  phi_deform_kt,edge_deform_kt, sigma_phi_deg, sigma_x, mt_radius)
% [guessed_coords, coordinate_distributions] = ...
%   mt_squashed_coords_from_phi(patch_phis, patch_xs, grid_xy, grid_dxy,  phi_deform_kt,edge_deform_kt, sigma_phi_deg, sigma_x, mt_radius)
%
%  One required argument: patch_phi's (in radians), a list of phi angles for each protofilament, as measured by patch refinement
%
%  Optional arguments grid_xy (integers) and grid_dxy (units of Angstroms)
%    define the extent and coarseness of the coordinate sampling grid;
%    the more distorted is the microtubule, the larger the extent of the grid needs to be.
%
%  Optional arguments phi_deform_kt (deg), edge_deform_kt (Angstroms), sigma_phi_deg (deg), mt_radius (Angstroms) 
%    define the geometry/bending resistance of the observed microtubules, as well as the 
%    characteristics of the measurement error. 
%  These are currently user inputs, but could be re-estimated as maximum likelihood quantities 
%    during successive iterations of the estimation loop.
%  Defaults:
%
%         phi_deform_kt = 2;               % Amount of relative rotation by a protofilament (degrees) against its neighbors 
%                                          %  that would give kT of distortion energy
%         edge_deform_kt = 0.5;            % Amount of deformation (Angstroms) required to give kT of distortion energy
%
%         sigma_phi_deg = 1                % Standard deviation of the error in measured axial rotation angles (phi), in degrees
%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin >= 3)
%%%%%%%%%%%%%%%%%%%%%%%%%
% Following 4 parameters define the extent and coarseness of the coordinate sampling grid;
%  the more distorted is the microtubule, the larger the extent of the grid needs to be.
%
% Computation/memory requirements grow with the square of nx*nz
% 
% These parameters can be set to lists of values that get applied in consecutive iterations.
% This allows a progression from a large, coarse grid to a small fine one.
%%%%%%%%%%%%%%%%%%%%%%%%%
  if(!isempty(grid_xy))
    nx = grid_xy(1,:);
    nz = grid_xy(2,:);
  else
    nx = [];
    nz = [];
  end
else
  nx = [];
  nz = [];
end
if(isempty(nx))
  nx = [11 11 11 11 11 11 11 11 11 11 11 11];
  nz = [11 11 11 11 11 11 11 11 11 11 11 11];
end

if(nargin >= 4)
  if(!isempty(grid_dxy))
    dx = grid_dxy(1);
    dz = grid_dxy(2);
  else
    dx = [];
    dy = [];
  end
else
  dx = [];
  dy = [];
end

if(isempty(dx))
  dx = [9 3 1 0.3 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 ];
  dz = [9 3 1 0.3 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 ];
end

%%%%%%%%%%%%%%%%%%%%%%%%
% The following parameters define the geometry/bending resistance of the observed microtubules, as well as the 
%  characteristics of the measurement error.  
%
% These are currently user inputs, but could be re-estimated as maximum likelihood quantities 
%   during successive iterations of the estimation loop.
%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 5)
  phi_deform_kt = [];
end

if(isempty(phi_deform_kt))
  phi_deform_kt = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ];  % Amount of relative rotation by a protofilament (degrees) against its neighbors 
                                        %  that would give kT of distortion energy
end

if(prod(size(phi_deform_kt)) == 1)
  phi_deform_kt = phi_deform_kt * ones(size(nx));  % Standard deviation of the error in measured protofilament x coordinate, in Angstroms
end

if(nargin < 6)
  edge_deform_kt = [];
end

if(isempty(edge_deform_kt))
  edge_deform_kt = [9 3 1 0.3 0.1 0.03 0.03 0.03 0.03 0.03 0.03 0.03 0.03 0.03 0.03 0.03];            % Amount of deformation (Angstroms) required to give kT of distortion energy
end

if(nargin < 7)
  sigma_phi_deg = 1;          % Standard deviation of the error in measured protofilament axial rotation angle, in degrees
end

if(nargin < 8)
  sigma_x = [];
end

if(isempty(sigma_x))
  sigma_x = 1 * ones(size(nx));  % Standard deviation of the error in measured protofilament x coordinate, in Angstroms
end

if(prod(size(sigma_x)) == 1)
  sigma_x = sigma_x * ones(size(nx));  % Standard deviation of the error in measured protofilament x coordinate, in Angstroms
end

if(prod(size(nx)) > 1)
  sigma_x(1) = nx(1) * dx(1) / 4;
end

if(nargin < 9)
  mt_radius = 120;
end

kT = 0.6;               % kcal/mol, although units are arbitrary

if(!isempty(patch_xs))
  n_pfs = prod(size(patch_xs));
%  phi_offset = 0;
end

if(!isempty(patch_phis))
  n_pfs = prod(size(patch_phis));
  patch_phis = mod([patch_phis patch_phis(1)], 2*pi);
%  phi_offset = fix_rollover_general(mod(patch_phis, 2*pi/n_pfs), 0, 2*pi/n_pfs);
%  phi_offset = sum(phi_offset)/n_pfs;
  phi_offset = patch_phis(1);
  patch_phi_est_2avg = zeros([1 n_pfs]);
  for ind=1:n_pfs
    next_ind = ind+1;
    if(next_ind > n_pfs)
      next_ind = 1;
    end
    patch_phi_est_2avg(ind) = avg_angle(patch_phis(ind),patch_phis(next_ind));
  end
  patch_phi_est_2avg = mod(patch_phi_est_2avg, 2*pi);
else
  phi_offset = 0;
end

phis = 0:n_pfs-1;
phis = phis * 2*pi/n_pfs + phi_offset;

% save('phi.mat', 'patch_phi_est_2avg');

edge_spring_constant = kT ./ (edge_deform_kt.^2);     % Spring constant, in units of kT/A^2
phi_spring_constant =  kT ./ ((phi_deform_kt*pi/180).^2);     % Spring constant, in units of kT/deg^2
sigma_phi = sigma_phi_deg*pi/180;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate starting model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theoretical_edge_length = sqrt(2*mt_radius^2 - 2*mt_radius^2 * cos(2*pi/n_pfs));
edge_length = theoretical_edge_length;
mloop_edge = edge_length * mt_radius / sqrt(mt_radius^2 - (edge_length/2)^2);

xz = mt_radius * [cos(phis) cos(phis(1)); sin(phis) sin(phis(1))]';

guessed_coords = xz;

xtest = mt_radius * cos(phis);
ztest = mt_radius * sin(phis);

phis2 = circshift(phis',-1)';
xtest2 = circshift(xtest',-1)';
ztest2 = circshift(ztest',-1)';

mloop1x = xtest + mloop_edge/2 * cos(phis+pi/2);
mloop1z = ztest + mloop_edge/2 * sin(phis+pi/2);
mloop2x = xtest2 + mloop_edge/2 * cos(circshift(phis',-1)'-pi/2);
mloop2z = ztest2 + mloop_edge/2 * sin(circshift(phis',-1)'-pi/2);
dlmwrite('temp.txt', [mloop1x; mloop1z; mloop2x; mloop2z;]', ' ');

update_progress_bar(0, prod(size(nx)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For multiple iterations...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iter=1:prod(size(nx))

  coordinate_distributions = zeros([prod(size(nx)) n_pfs 3 nx(iter)*nz(iter)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up coordinate grids
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  update_progress_bar(iter, prod(size(nx)));

  sample_coords = zeros([nx(iter) nz(iter) 2 n_pfs]);

  for pf=1:n_pfs+1
     [grid_x,grid_z] = ndgrid(guessed_coords(pf, 1) + (-floor(nx(iter)/2):floor(nx/2)) * dx(iter), ...
                              guessed_coords(pf, 2) + (-floor(nz(iter)/2):floor(nz/2)) * dz(iter));
    sample_coords(:,:,1,pf) = grid_x;
    sample_coords(:,:,2,pf) = grid_z;
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute transfer matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  tms = zeros([nx(iter)*nz(iter), nx(iter)*nz(iter), n_pfs]);
%  tms_phi_spring_reestimate = zeros([nx(iter)*nz(iter), nx(iter)*nz(iter), n_pfs]);

  for pf=1:n_pfs
    next_pf=pf+1;

    x1 = sample_coords(:,:,1,pf)(:);
    z1 = sample_coords(:,:,2,pf)(:);
    x2 = sample_coords(:,:,1,next_pf)(:);
    z2 = sample_coords(:,:,2,next_pf)(:);

    x1 = repmat(x1, [1, nx(iter)*nz(iter)]);
    z1 = repmat(z1, [1, nx(iter)*nz(iter)]);
    x2 = repmat(x2', [nx(iter)*nz(iter), 1]);
    z2 = repmat(z2', [nx(iter)*nz(iter), 1]);

    dist = sqrt((x1-x2).^2 + (z1-z2).^2);
    dist = reshape(dist, [nx(iter)*nz(iter),nx(iter)*nz(iter)]);

    tms(:,:,pf) = exp(-0.5 * edge_spring_constant(iter) * (dist-edge_length).^2 / kT);

    if(!isempty(patch_phis))
%      tms(:,:,pf) = tms(:,:,pf) .* exp(-phi_spring_constant(iter) * (mod(atan2(z2-z1, x2-x1) - pi/2 - patch_phi_est_2avg(pf), 2*pi)).^2);
       tms(:,:,pf) = tms(:,:,pf) .* exp(-phi_spring_constant(iter) * (mod(atan2(z2-z1, x2-x1) - pi/2 - 2*pi/n_pfs/2 - patch_phis(pf) + pi, 2*pi) - pi).^2) .* ...
                                    exp(-phi_spring_constant(iter) * (mod(atan2(z2-z1, x2-x1) - pi/2 + 2*pi/n_pfs/2 - patch_phis(pf+1) + pi, 2*pi) - pi).^2);

%        mloop1x = x1 + mloop_edge/2 * cos(patch_phis(pf)+pi/2);
%        mloop1z = z1 + mloop_edge/2 * sin(patch_phis(pf)+pi/2);
%        mloop2x = x2 + mloop_edge/2 * cos(patch_phis(next_pf)-pi/2);
%        mloop2z = z2 + mloop_edge/2 * sin(patch_phis(next_pf)-pi/2);

%        dist2 = (mloop1x - mloop2x).^2 + (mloop1z - mloop2z).^2;
%        dist2 = reshape(dist2, [nx(iter)*nz(iter),nx(iter)*nz(iter)]);
%        writeSPIDERfile(sprintf('dist2%d.spi', pf), dist2);

%        tms(:,:,pf) = tms(:,:,pf) .* exp(-0.5 * edge_spring_constant(iter) * ( (mloop1x - mloop2x).^2 + (mloop1z - mloop2z).^2));

    else
      tms(:,:,pf) = tms(:,:,pf) .* exp(-phi_spring_constant(iter) * (mod(atan2(z2-z1, x2-x1) - pi/2 - 2*pi/n_pfs/2 - atan2(z1,x1) + pi, 2*pi) - pi).^2) .* ...
                                   exp(-phi_spring_constant(iter) * (mod(atan2(z2-z1, x2-x1) - pi/2 + 2*pi/n_pfs/2 - atan2(z2,x2) + pi, 2*pi) - pi).^2);
    end

    if(!isempty(patch_xs))
      tms(:,:,pf) = tms(:,:,pf) .* exp(-1/sigma_x(iter)^2 * ((x1 - patch_xs(pf)).^2));
    end

%    tms_phi_spring_reestimate(:,:,pf) = tms(:,:,pf) .* (mod(atan2(z2-z1, x2-x1) - pi/2 + 2*pi/n_pfs/2 - patch_phis(pf+1) + pi, 2*pi) - pi).^2;
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute transfer matrix products, which give likelihoods as the trace of the product matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  guessed_coords = zeros([n_pfs 2]);
%  phi_spring_reestimate_array = zeros([1 n_pfs]);
  for target_pf=1:n_pfs
    for ind=1:n_pfs
      t = mod(ind+target_pf-2,n_pfs)+1;
      if(ind == 1)
        tm_prod = tms(:,:,t);
%        tm_phi_prod = tms_phi_spring_reestimate(:,:,t);
      else
% Normalize matrix product terms by nx*nz to keep values from blowing up
        tm_prod = tm_prod * tms(:,:,t) / (nx(iter)*nz(iter));
%        tm_phi_prod = tm_phi_prod * tms_phi_spring_reestimate(:,:,t) / (nx(iter)*nz(iter));
      end
    end

    output_scores = tm_prod .* eye(size(tm_prod));
    output_scores = output_scores(find(eye(size(tm_prod))));

%    phi_spring_reestimate = tm_phi_prod .* eye(size(tm_prod));
%    phi_spring_reestimate = phi_spring_reestimate(find(eye(size(tm_prod))));

    output_scores = output_scores / sum(output_scores(:));
%    phi_spring_reestimate_array(target_pf) = sum(phi_spring_reestimate(:)) / sum(output_scores(:));

    pf=target_pf;
    x1 = sample_coords(:,:,1,pf)(:);
    z1 = sample_coords(:,:,2,pf)(:);
    x1 = repmat(x1, [1, nx(iter)*nz(iter)]);
    z1 = repmat(z1, [1, nx(iter)*nz(iter)]);
    output_x = x1(find(eye(size(tm_prod))));
    output_z = z1(find(eye(size(tm_prod))));

    x_expectation = sum(output_x .* output_scores) / sum(output_scores(:));
    z_expectation = sum(output_z .* output_scores) / sum(output_scores(:));

    guessed_coords(target_pf, :) = [x_expectation z_expectation];
    coordinate_distributions(iter, target_pf, :, :) = [output_x'; output_z'; output_scores'];

%    dlmwrite(sprintf('results_data/tm%d.txt', target_pf), [output_x'; output_z'; output_scores']', ' ');
  end

%  phi_spring_reestimate = sum(phi_spring_reestimate_array(:))/n_pfs

  guessed_coords = [guessed_coords; guessed_coords(1,:)];
%  dlmwrite(sprintf('xz_predict_iter%d.txt', iter), guessed_coords, ' ');

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% End of multiple iterations...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% dlmwrite('test.txt', [mt_radius * sqrt(sum(diff([tempc1; tempc1(1,:)]).^2,2)) sqrt(sum(diff([guessed_coords; guessed_coords(1,:)]).^2,2))], ' ');

% score_rigid = sqrt( sum( (guessed_coords(1:n_pfs,1) - xz(1:n_pfs,1)).^2)/n_pfs)
