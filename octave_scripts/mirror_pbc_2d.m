function out_stack = mirror_pbc_2d(stack)

m = size(stack);
if(prod(size(m)) < 3)
  n_images = 1;
else
  n_images = m(3)
end

out_stack = zeros(2*m(1:2), n_images);

out_stack(1:m(1), 1:m(2), :) = stack;
out_stack(m(1)+1:2*m(1), 1:m(2)) = stack(m(1):-1:1,:,:);
out_stack(1:m(1), m(2)+1:2*m(2)) = stack(:,m(2):-1:1, :);
out_stack(m(1)+1:2*m(1), m(2)+1:2*m(2)) = stack(m(1):-1:1,m(2):-1:1);
