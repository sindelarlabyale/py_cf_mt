#!/bin/bash

export chuff_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# remove old chuff directory
export PATH=$(echo ${PATH} | awk -v RS=: -v ORS=: '/chuff/ {next} {print}' | sed 's/:*$//')
exec_path="linux"
# remove eman2 python from path
export PATH=$(echo ${PATH} | awk -v RS=: -v ORS=: '/extlib/ {next} {print}' | sed 's/:*$//')
# add 3rd party executable for machine specific
export PATH="$chuff_dir/$exec_path:$PATH"

# add commands to PATH
export PATH="$chuff_dir/commands:${PATH}"

# add python libraries
if [ -z ${PYTHONPATH+x} ]; then
    PYTHONPATH=""
fi

export PYTHONPATH="$chuff_dir/python_scripts:$chuff_dir/python_scripts/lib"
# :${PYTHONPATH}"

if [ -d "$chuff_dir/python_scripts/cf" ]; then
<<<<<<< HEAD
    source $chuff_dir/python_scripts/cf/bin/activate
=======
    source "$chuff_dir/python_scripts/cf/bin/activate"
>>>>>>> findkin
fi
