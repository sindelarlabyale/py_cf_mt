/* 
   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/

int rotate(double *input, double *output, unsigned long rows, 
	   unsigned long cols, double angle);

int winrot(double *input, double *output, unsigned long rows, 
	   unsigned long cols, double angle, int wlength, double thrsh, 
	   int method);
