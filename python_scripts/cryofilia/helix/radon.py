'''
    Radon align to find the seam position for microtubule
    In-plane rotation of helical particles using the radon transform, via the method of:
    Li et al (2002), Structure 10 (1317-1328)

    Power spectrum of microtubule has layer lines, so do other helical symmetry.
    layer line is usually perpendicular to the helical axis, and in certain
    distance  of the center of power spectrum. So use radon transform to find
    the maximum rotation that give the maximum value at the layer line distance

    Usages:
    angles, est_distance, est_disatances = inplane_align(stack_np, pixel_size)

'''
import numpy as np
from tqdm import tqdm
import cryofilia.util2d as  util2d
from cryofilia.EMImage import EMImage, TestImage
from relion import Parameter

def inplane_align(stack_np, pixel_size,
                  target_resolution_radon=40,
                  fourier_dim_radon=64,
                  d_angle=0.25,
                  search_halfw_radon=15,
                  #layer_line_radius=128,
                  layer_line_resolution=None,
                  max_spacing_uncertainty=0.1,
                  ori_psi=0.,
                  #debug=False
                 ):
    """In-plane rotation of helical particles using the radon transform, via the method of:
    Li et al (2002), Structure 10 (1317-1328)

    The helical repeat distance is also estimated by measuring the projected 1D position
    of a given layer line, if layer_line_resolution is specified.

    Return values:
        return est_angles, est_repeat_distance, est_repeat_distances
        (argument types: array, float, array)
    """
    if isinstance(stack_np, np.ndarray):
        stack_np = EMImage(stack_np)
    n = stack_np.zsize
    
    # make original psi list
    try:
        _ = ori_psi[0]
    except TypeError:
        ori_psi = [ori_psi] * n
    
    # get a copy of original image
    ptcls = [EMImage(stack_np.data[i, :, :]) for i in range(n)]
    rd = Radon()
    #pylint: disable = attribute-defined-outside-init
    rd.pixel_size = pixel_size
    rd.fourier_dim_radon = fourier_dim_radon
    rd.d_angle = d_angle
    rd.layer_line_resolution = layer_line_resolution
    rd.max_spacing_uncertainty = max_spacing_uncertainty
    rd.search_halfw_radon = search_halfw_radon
    rd.target_resolution_radon = target_resolution_radon
    
    est_angles, est_repeat_distance, est_repeat_distances = rd.align_stack(
        ptcls, return_repeat_distance=True,
        ori_psis=ori_psi)
    
    return est_angles, est_repeat_distance, est_repeat_distances

class RadonParameter(Parameter):
    ''' Radon alignment parameters
    '''
    _labels = {
        "search_halfw_radon": ["searchHalfwRadon", float, 
                    "angle search distance around original angle"],
        "pixel_size" : ['pixelSize', float, 'pixel Size of original particle'],
        # pixel_size_radon is the pixelSize of radon Transform
        # is the pixel size for get power specturm of real image
        # is half of target_resolution
        "target_resolution_radon" : ['targetResolutionRadon', float, 
                    'target resolution of power spectrum used for radon transform'],
        # fourier_dim_radon is the dimension of the radon transform
        "fourier_dim_radon" : ['fourierDimRadon', int, "Fourier dimension"],
        "layer_line_resolution" : ['layerLineResultion', float, 
                                   'Layerline Resolution, in Angstrom'],
        "max_spacing_uncertainty" : ['maxSpacingUncertainty', float, 
                                     "maxSpacingUncertainty"],
        "d_angle"   : ["dAngle", float, ""]
        }
    def __init__(self):
        super(RadonParameter, self).__init__("RadonParameter")

class Radon(RadonParameter):
    ''' Radon class to get the angle
        Usage:
        r = Radon()
        r.pixel_size = 0.65
        r.target_resolution_radon = 20
        r.layer_line_resolution = 40
        r.search_halfw_radon = 15
        r.d_angle = 0.1

    '''
    def __init__(self, use_gpu=False):
        super(Radon, self).__init__()
        self._pw_mask = None
        self._angles = None
        self.sum_pw = None
        # set default parameters
        self.fourier_dim_radon = 64
        self.max_spacing_uncertainty = 0.1
        self.d_angle = 0.1
        self.search_halfw_radon=15
        self.target_resolution_radon=5.
    def align_particle(self, ptcl, ori_psi=0.,
                       return_repeat_distance=False):
        ''' Get inplace rotation using radon transform
        '''

        if self.pixel_size <= 0.:
            raise Exception("pixel_size should be set")
        #pylint: disable=access-member-before-definition, attribute-defined-outside-init
        if self.target_resolution_radon <= 0.:
            self.target_resolution_radon  = self.pixel_size * 2

        #self.pixel_size_radon = self.target_resolution_radon / 2
        if self.layer_line_resolution <= 0.:
            raise Exception("layer line radius should be set")

        if self.layer_line_resolution < self.target_resolution_radon:
            raise Exception("target resolution radon  must be smaller than layer_line_resolution ")
        # the radius in (0,0.5)
        self.layer_line_radius = self.pixel_size_radon / self.layer_line_resolution
        # resample the image to make pixel_size : pixel_size_radon
        ptcl.resample(int(ptcl.xsize * self.pixel_size / self.pixel_size_radon / 4) * 4)
        ptcl.normalize()
        # to here, the pixel size of ptcl is self.pixel_size_radon
        # pad to original ptcl size to get oversampled power spectrum.
        # could use smaller size if original size is too large
        # This defines how the pw is resampled.
        ptcl = ptcl.clip(ptcl.xsize * 4)

        # get the power spectrum.
        ori_pw = abs(np.fft.fftshift(ptcl.fftdata))
        self.layer_line_radius_pixel = int(self.layer_line_radius * ori_pw.shape[0])
        self.fourier_dim_radon = int(1.5 * self.layer_line_radius_pixel * 2)
        # The nyquist frequency is 2 * pixel_size_radon = target_resolution
        # to calculate the layer line position,
        # use: r = N * apix / res, where N is the ori_pw size,
        # apix is the pixel_size_radon,
        # res is the layer line resolution
        # clip to size for radon transform
        pw = EMImage(ori_pw).clip(self.fourier_dim_radon).data
        # mask the power spectrum to remove zero frequency
        # and noise outside certain resultion
        pw *= self.pw_mask.data
        # create buffer for radon transformv
        angles = self.angles + ori_psi
        radon_image = self.radon_transform(EMImage(pw), angles = angles)
        radon_image = radon_image.data
        # debug write out radon transform
        #EMImage(radon_image).append_to_mrc('radon.mrc')
        _, angle_index_fitted = \
            util2d.peakfind1d(radon_image[:, radon_image.shape[1]/2])[0:2]
        a = int(angle_index_fitted)
        psi = (angle_index_fitted - a) * self.d_angle + angles[a]
        pw = EMImage(ori_pw)
    
        pw.rotate2d(psi)
        if self.sum_pw is None:
            self.sum_pw = pw
        else:
            self.sum_pw += pw
        if return_repeat_distance:
            return psi, self.get_repeat_distance(pw)
        else:
            return psi

    def radon_transform(self, img, radius=None, angles=None):
        ''' Generate radon transform with angles
        '''
        rd_size = img.xsize
        if radius is not None and radius > 0:
            rd_size = min(img.xsize, radius * 2 + 1)
        img = img.clip(img.xsize * 2)
        img.data = np.fft.fftshift(img.data)
        ft = np.fft.fftshift(np.fft.fft2(img.data)) / 4 / img.xsize
        idx_x = np.arange(img.xsize) - img.xsize / 2
        idx_y = np.arange(img.ysize) - img.ysize / 2
        import scipy.interpolate
        # make the interpolation data
        rgi = scipy.interpolate.RegularGridInterpolator(
            [idx_y, idx_x], ft,
            bounds_error = False, fill_value = 0
            )
        if angles is None:
            angles = range(180)
        # get the coordinates
        angles = np.deg2rad(angles).reshape([len(angles), 1])
        _, X = np.mgrid[0:len(angles), -(rd_size / 2):(rd_size+1)/2]
        x = X * np.cos(-angles) * 2
        y = X * np.sin(-angles) * 2
        coords = np.stack([y, x], axis=2)
        rt = rgi(coords)
        rt = np.fft.ifftshift(rt, axes = 1)
        rt = np.fft.ifft(rt, axis=1).real
        rt = np.fft.ifftshift(rt, axes=1)
        return EMImage(rt)

    def align_stack(self, stack, stack_info=None,
                    ori_psis=None,
                    return_repeat_distance=True,
                    progress_bar=None):
        ''' Align list of particles. Stack should be a list of particles in EMImage format
        '''
        self.reset()
        # check the stack format, convert to list of EMImage if not
        
        try:
            _ = stack[0]
        except TypeError:
            stack = [stack]
        if len(stack) == 1:
            if isinstance(stack[0], np.ndarray):
                stack[0] = [EMImage(stack[0])]
            if stack[0].zsize > 1:
                stack = [EMImage(stack.data[i,:,:]) for i in range(stack[0].zsize)]
        if progress_bar is None:
            import sys
            pg_bar = tqdm(total = len(stack), desc="radon", disable=sys.stdout.fileno() != 1)
        else:
            pg_bar = progress_bar
        psis = []
        all_rd = []
        
        for i in range(len(stack)):
            ptcl = stack[i]
            if stack_info is not None and stack_info[i].hasKey('AnglePsi'):
                ori_psi = stack_info[i]['AnglePsi']
            elif ori_psis is not None:
                try:
                    ori_psi = ori_psis[0]
                except TypeError:
                    ori_psi = ori_psis
            else:
                ori_psi = 0

            psi, rd = self.align_particle(ptcl,
                                          ori_psi,
                                          return_repeat_distance = True)
            if stack_info is not None:
                stack_info[i]['AnglePsi'] = psi
            psis.append(psi)
            all_rd.append(rd)
            pg_bar.update(1)

        repeat_distance = self.get_repeat_distance(self.sum_pw)
        if return_repeat_distance:
            return psis, repeat_distance, all_rd
        else:
            return psis
        if progress_bar is None:
            pg_bar.close()
            
    def reset(self):
        ''' Reset pw_sum to default
        '''
        #pylint: disable=attribute-defined-outside-init
        self.pw_sum = None

    def get_repeat_distance(self, pw):
        ''' Get the repeat distance by layer line
            repeat_distance = boxsize * pixel_size / layerlineresolution
        '''
        pixel_size_ll = self.pixel_size_radon
        layer_line_radius = self.layer_line_radius_pixel
        strip_ll = np.sum(pw.data, 0)
        #pylint: disable=access-member-before-definition,attribute-defined-outside-init
        if self.max_spacing_uncertainty <= 0.:
            self.max_spacing_uncertainty = 0.1
        strip_ll[0:strip_ll.shape[0]/2 + int((1-self.max_spacing_uncertainty) * layer_line_radius)] = 0
        strip_ll[strip_ll.shape[0]/2 + int((1+self.max_spacing_uncertainty) * layer_line_radius):] = 0
        _, ll_index_fitted = util2d.peakfind1d(strip_ll)[0:2]
        est_repeat_distance_r = ll_index_fitted - strip_ll.shape[0]/2
        est_repeat_distance = strip_ll.shape[0] / est_repeat_distance_r * pixel_size_ll
        return est_repeat_distance

    @property
    def pw_mask(self):
        ''' Generate the pw_mask when first time use.
        Reset when the parameter used here is changed.
        depends on parameter: fourier_dim_radon, pixel_size_radon
        '''
        if self._pw_mask is None:
            self._pw_mask = TestImage.gaussian(
                [self.fourier_dim_radon, self.fourier_dim_radon],
                radius = -40/self.pixel_size_radon,
                width = 10. / self.pixel_size_radon)
            self._pw_mask *= TestImage.gaussian(
                [self.fourier_dim_radon, self.fourier_dim_radon], width = 2)
        return self._pw_mask

    @property
    def angles(self):
        ''' Generate the angle list for radon transform.
        Depends on parameters:
            search_halfw_radon, d_angle
        '''
        if self._angles is None:
            self._angles = np.arange(0, 2 * self.search_halfw_radon + 1,
                                     self.d_angle) - self.search_halfw_radon
        return self._angles

class MtRadon(Radon):
    def __init__(self, **kwargs):
        use_gpu = kwargs.pop('use_gpu', False)
        super(MtRadon, self).__init__(use_gpu=use_gpu)
        self.layer_line_resolution = 80
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
