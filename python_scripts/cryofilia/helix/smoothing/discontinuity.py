'''
Discontinuity detection module

@author: xueqi
'''
import numpy as np
from cryofilia.euler import Euler

def calculate_angle_directions(angles):
    """ Calculate the direction using angles
    """
    dirs = np.zeros([angles.shape[0], 3])
    unit_vec = np.array([0, 0, 1])
    for idx in range(angles.shape[0]):
        dirs[idx] = Euler(angles[idx, 0], angles[idx, 1], angles[idx, 2]).transform(unit_vec)
    return dirs

def calculate_directions(coords):
    """ Calculate directions for each point in coords.
    direction of each point is calculated with next point. 
    The last point has same direction of the previous one
    Don't do sanity check. Caller should make sure the input in correct format
    :param coords: 3D coordinates. In numpy.ndarray format
    :type coords: ndarray
    :return: The directions for each coordinate, represented with unit vectors
    :rtype: ndarray
    """
    vecs = coords[1:, :] - coords[:-1, :]
    vecs = np.insert(vecs, -1, vecs[-1, :], axis=0)
    # calculate the norms
    dists = np.linalg.norm(vecs, axis=1)
    vecs = (vecs.T / dists.T).T
    return vecs

def calculate_direction_change(coords):
    """ The direction change is defined as the angles in radius between
    from last point to this point  and this point to next point.
    The direction change of first point and last point is always 1.
    dir = dot(x_{i} - x_{i-1}, x_{i+1} - x{i})
    """
    directions = calculate_directions(coords)
    dot_prod = directions[1:, :] * directions[:-1, :]
    dot_prod = np.sum(dot_prod, axis=1)
    result = np.ones(coords.shape[0])
    result[1:] = dot_prod
    return result

def get_dist_from_last_pixel(coords):
        """ fist one is always the same as second one
        """
        dist = np.zeros(coords.shape[0])
        dist[1:] = np.linalg.norm(coords[1:,:3] - coords[:-1, :3], axis=1)
        dist[0] = dist[1]
        return dist
    
def dist_discontinuity(coords, threshold=0.):
    """ Get distance discontinuities
    :param coords: 3D coordinates. In numpy.ndarray format
    :type coords: ndarray
    :param threshold: The threshold of angle change in degree to 
        identfy as discontiniuty. (in [0, 180))
    :param threshold: float
    :return: The directions for each coordinate, represented with unit vectors
    :rtype: ndarray of bool
    """
    if coords.shape[0] == 1:
        return np.array([False], dtype=np.bool)
    dist = get_dist_from_last_pixel(coords)
    dist_diff = np.zeros(dist.shape[0])
    dist_diff[1:] = dist[1:] - dist[:-1]
    return dist_diff > threshold

def coord_discontinuity(coords, threshold=10.):
    """ Calculate discontinuity of coordinates.
    discontinuity is defined as the direction change at this point
    exceeds certain value
    :param coords: 3D coordinates. In numpy.ndarray format
    :type coords: ndarray
    :param threshold: The threshold of angle change in degree to 
        identfy as discontiniuty. (in [0, 180))
    :param threshold: float
    :return: The directions for each coordinate, represented with unit vectors
    :rtype: ndarray of bool
    """
    if coords.shape[0] < 3:
        return np.zeros(coords.shape[0], dtype=np.bool)
    threshold = np.cos(threshold * np.pi / 180.)
    dir_change = calculate_direction_change(coords)
    return dir_change < threshold

def twist_discontinuity(phis, threshold=10.):
    """ Calculate twist discontinuity
    twist change is expected to be slow or none.
    if delta phi after and before this point is larger than threshold, 
    it is identified as discontinuity
    """
    phi_delta = phis[1:] - phis[:-1]
    phi_delta_delta = np.zeros(phis.shape[0])
    phi_delta_delta[1:-1] = np.abs(
        np.mod(phi_delta[1:] - phi_delta[:-1] + 180, 360) - 180)
    return phi_delta_delta > threshold
