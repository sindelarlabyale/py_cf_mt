from .relion import *
from .parameter import Parameter, MPIParameter
import os

_labels_file = os.path.join(os.path.dirname(__file__), "labels.txt")

_types = {"float" : float, "int" : int, "bool" : bool, 'str':str
        }

# import the default labels. This is from relion
for line in open(_labels_file):
    line = line.strip()
    tmp = line.split()
    if len(tmp) < 3: 
        logging.warn("labels.txt file corrupted")
        continue
    lblName = tmp[0]
    pyType = _types[tmp[1]]
    lblComment = " ".join(tmp[2:])
    EMDLabel.addLabel(lblName, pyType, lblComment)

# MT related
EMDLabel.addLabel('pfs', int, "Number of Protofilament")
EMDLabel.addLabel('starts', int, "Number of starts")
EMDLabel.addLabel('crossCorrelation', float, "Cross Correlation Score")
EMDLabel.addLabel('seampos', int, "Seam position")
