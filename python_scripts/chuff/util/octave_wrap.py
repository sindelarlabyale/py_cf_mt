import os
import logging
from oct2py import Oct2Py, get_log
from chuff import chuff_dir
import tempfile
OCTAVE_INITED = False
octave = Oct2Py(tempfile.mkdtemp())
octave_paths = [
    os.path.join(chuff_dir, 'octave_scripts'),
    os.path.join(chuff_dir, 'octave_mex_linux'),
    ]

logger = logging.getLogger(__name__)

def init_octave():
    global OCTAVE_INITED
    if not OCTAVE_INITED:
        logger.debug('initialize octave')
        octave.logger = get_log('new_log')
        octave.logger.setLevel(logging.INFO)
        chuff_dir = os.environ['chuff_dir']
        for pth in octave_paths:
            octave.addpath(pth)
        OCTAVE_INITED = True
        logger.debug('octave initilized')

def get_octave():
    ot = Oct2Py(tempfile.mkdtemp())
    for pth in octave_paths:
        ot.addpath(pth)
    ot.logger = get_log('new_log')
    ot.logger.setLevel(logging.INFO)
    return ot
