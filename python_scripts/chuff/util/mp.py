import copy_reg
import types
import os, sys
from multiprocessing import Pool, Manager, Lock, cpu_count
from multiprocessing.managers import BaseManager
import threading

class OutputProxy(threading.Thread):
    def __init__(self, inq, outq):
        self.inq = inq
        self.outq = outq

    def run(self):
        pass

class MPManager(BaseManager):
    pass

class MP(object):
    ''' Multi processor protocol. Use instance.parallel_job(*args, **kwargs) to run parallel.
    '''
    def __init__(self, ncpus = 0):
        import sys
        super(MP,self).__init__()
        self.ncpus = ncpus
        self.results = []
        self.output_lock = None
        self.print_output = True
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        self.output = []
        self.clses = []
    
    def start(self):
        self.run()

    def run(self):
        pass
    def dummy_func(self, func, *args, **kwargs):
        import multiprocessing as mp
        try:
            self.output_lock.acquire()
            if self.print_output[0]:
                print_output = True
                self.print_output[0] = False
            else:
                print_output = False
            self.output_lock.release()
            # kick all stdout
            if not print_output:
                self.stdout = open(os.devnull, 'w')
#                self.stderr = open(os.devnull, 'w')
                tmp_stdout = sys.stdout
                sys.stdout = self.stdout
                # set root logger handler level to warn
                import logging
                root_logger = logging.getLogger()
                log_lvl = root_logger.level
                root_logger.setLevel(logging.WARNING)
<<<<<<< HEAD
=======
            if isinstance(func, str):
                if func.startswith('self.'):
                    func = getattr(self, func[5:])
>>>>>>> findkin
            result = func(*args, **kwargs)
            # restore stdout
            if not print_output:
                root_logger.setLevel(log_lvl)
                sys.stdout = tmp_stdout
        except Exception as e:
            print(sys.exc_info())
            self.output_lock.acquire()
            self.print_output[0] = True
            self.output_lock.release()
            raise e
        else:
            self.output_lock.acquire()
            if print_output:
                self.print_output[0] = True
            self.output_lock.release()
            return result

    def parallel_job(self, func, list_of_args, callback = None):
        ''' Run parallel job 
        :param list_of_args: The list of (args, kwargs) should pass to func. 
                This could be a generator

        :return: The list of all result from all processes.
        '''
        import signal
        ncpus = self.ncpus
        if ncpus <= 0:
            ncpus = cpu_count()
        # https://stackoverflow.com/questions/11312525/catch-ctrlc-sigint-and-exit-multiprocesses-gracefully-in-python Catch Ctrl+C signal. #TODO: Do not work...
        m = Manager()
        self.output_lock = m.Lock()
        self.print_output = m.list()
        self.print_output.append(True)
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = Pool(processes = ncpus)
        signal.signal(signal.SIGINT, original_sigint_handler)
        results = []
        async_results = []
        try:
            for args, kwargs in list_of_args:
                args = tuple([func] + list(args))
                import time
                async_results.append(pool.apply_async(self.dummy_func, args, kwargs, callback = callback))
            for async_r in async_results:
                res = async_r.get(60 * 60 * 24)
                results.append(res)
                if callable(callback):
                    callback(res)
        except KeyboardInterrupt:
            print("Control C pressed, terminating all workers")
            pool.terminate()
        else:
            print("Normal terminate for parallel job")
            pool.close()
        pool.join()
        return results
    
    def mkdir(self, d):
        ''' Make directory in parallel
        '''
        try:
            os.mkdir(d)
        except OSError as e:
            if e.errno != 17: # File exists
                raise e
            
    def __getstate__(self):
        st = self.__dict__.copy()
        del st['stderr']
        del st['stdout']
        return st

    def __setstate__(self, st):
        self.__dict__.update(st)

    def register(self, cls):
        ''' Register a class for passing
        '''
        self.clses.append(cls)

# add support for pickling/unpickling method
# https://bytes.com/topic/python/answers/552476-why-cant-you-pickle-instancemethods
# be careful the instance passed is not the instance call the function if use Multiprocessing or MPI
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)
