import os
import re
import logging
import random
import shutil
import glob
import time

from copy import deepcopy
from .frealign_par import FrealignParameter
from relion import Metadata

logger = logging.getLogger(__name__)
FREALIGN_INFO_FILE = 'info.txt'
FREALIGN_INPUT_FILE_INFO_FILE = 'frealign_input_file_info.txt'
merge3d = 'merge_3d'

class FrealignNotCompleted(Exception):
    def __init__(self, *args, **kwargs):
        super(FrealignNotCompleted, self).__init__(*args, **kwargs)

class Frealign(object):
    '''
        Frealign info stored in Frealign.info, instance
        
        Frealign header is the root of the output volumes
        
        Frealign particles are tracked by particle stack, in member stacks, which is dictionary of 
            key: the stack name,
            value: The FrealignParameter for this stack.
        Each FrealignParameter should associated with one particle stack. 
        
        
    '''
    def __init__(self, frealign_dir = None, info = None, 
                 stacks = None, frealign_round = 0):
        '''
            a frealign running object, containing all infos needed for chuff_frealign
        '''
        self.frealign_dir = frealign_dir

        if info is None:
            self.info = {}
        self.frealign_round = frealign_round
        self.stacks = {}
        self.m_mg_id = None
        self.image_extension = "mrc"
        if os.path.exists(self.get_info_file()):
            self.read_info_file(self.get_info_file())
        
    def get_last_round(self):
        '''
            return last round run.
            Need to have the output volume to be the completed round
            -1 if not run any
        '''
        output_header = self.get_output_header()
        frealign_round = 1
        while os.path.exists("%s_%s.%s" %(output_header, 
                                          frealign_round, 
                                          self.get_output_ext())):
            frealign_round += 1
        return frealign_round - 1
    
    def get_output_header(self):
        '''
            get output header
            @return: The output header for this frealign instance
        '''
        if not 'output_header' in self.info:
            self.read_info_file(self.get_info_file())
        return self.info['output_header']
    
    def get_output_ext(self):
        '''
            get the extension for the volume and ptcl that frealign used
        '''
        return self.image_extension
    
    def set_frealign_dir(self, new_frealign_dir):
        ''' Set a new frealign dir. 
            This will set the new frealign dir, and change the output_header info
        @param new_frealign_dir: The new frealign dir to use. 
        '''
        self.frealign_dir = new_frealign_dir
        if 'output_header' in self.info:
            self.info['output_header'] = os.path.join(new_frealign_dir, os.path.basename(self.info['output_header']))

    def read(self, frealign_dir = None, frealign_round = None):
        '''
            read a frealign instance from directory.
            @param frealign_dir: The root directory of the frealign
            @param frealign_round: The round of parameter to read
        '''
        if frealign_dir is None:
            frealign_dir = self.frealign_dir
        if frealign_round == None:
            frealign_round = self.frealign_round
        if not os.path.exists(frealign_dir):
            raise Exception, "No frealign_dir %s found" % frealign_dir
        
        self.read_info_file()
        self.read_frealign_input_file_info()
        self.read_particle_parameter(frealign_round)
        self.frealign_round = frealign_round
    
    def readResolution(self, frealign_round = None):
        
        frealign_round = frealign_round or self.frealign_round

        res_file = "%s_%d.res" % (self.info['output_header'], frealign_round)
        from frealign_resolution import FrealignResolution
        fRes = FrealignResolution(res_file)
        fsc05 = fRes.getFSC(0.5)
        return fsc05

    def write_parameter_files(self, frealign_round = None):
        ''' write all parameters to file for frealign_round
        @param frealign_round: The round to write. The input for frealign_round 
          is frelaign_round -1, so the result is write the _${frealign_round -1}
        '''
        filament_dir = os.path.join(self.frealign_dir, 'filament_data')
        if not os.path.exists(filament_dir):
            os.mkdir(filament_dir)
        if frealign_round is None:
            frealign_round = self.frealign_round
        for fp in self.stacks.values():
            par_file = self.get_par_file(fp.par_file, 
                                         frealign_round = frealign_round)
            fp.write_param_to_file(par_file)
    
    def get_par_file(self, par_root, frealign_round = None):
        '''Get the par file for par_root and frealign_round
        @param par_root: The root name of the par_file, taken from fp.par_file
        @param frealign_round: The par_file name for frealign_round.
        @return: The par_file name
        '''
        if frealign_round is None: frealign_round = self.frealign_round
        par_root = os.path.basename(par_root)
        par_root = re.sub("_\d+.par", "", par_root)
        par_file = "%s/%s_%d.par" % (self.get_par_dir(), par_root, frealign_round)
        return par_file
    
    def get_par_dir(self):
        return os.path.join(self.frealign_dir, 'filament_data')
    
    def get_info_file(self):
        '''
            get the frealign info file
        '''
        return os.path.join(self.frealign_dir, FREALIGN_INFO_FILE)
    
    
    def read_info_file(self, info_file = None):
        '''
        read frealign info file from chuff style. This function read the info to self.info, and return it.
        keys: output_header, voxel_size, bin_factor
        num_pfs, num_starts, microtubule, micrograph_pixels_per_repeat,
        helical_twist
        :param info_file: The info_file.txt in frealign_dir
        '''
        info_file = info_file or self.get_info_file()
        with open(info_file) as info_f:
            for line in info_f:
                if not line.strip(): continue
                tmp = line.strip().split()
                if len(tmp) < 2: continue
                key = tmp[0]
                value = line.strip()[len(key):].strip()
                if (    key.startswith('bin_factor') 
                     or key.startswith('microtubule') 
                     or key.startswith('num_pfs') 
                     or key.startswith('num_starts') ):
                    value = int(value)
                elif ( key.startswith('voxel') 
                    or key.startswith('helical')):
                    value = float(value)
                self.info[key] = value
        return self.info

    def write_info_file(self, info_file = None):
        '''
            write self.info to info file for chuff style
            :param info_file: info file to write
        '''
        if info_file is None:
            info_file = self.get_info_file()
        with open(info_file, 'w') as info_f:
            for key, value in self.info.items():
                info_f.write("%s %s\n" % (key, value))
    
    def get_output_volume(self, frealign_round = None):
        '''
            get output volume. 
            
            @param realign_round: The round to get. used for get last round of volume for reference

        '''
        if round is None: frealign_round = self.frealign_round
        if not 'output_header' in self.info:
            self.read_info_file()
        if 'output_header' in self.info:
            return "%s_%d.spi" % (self.info['output_header'], frealign_round)
        else:
            return "%s/%s_%d.spi" % (self.frealign_dir, os.path.basename(self.frealign_dir), frealign_round)

    def get_frealign_input_info_file(self):
        return os.path.join(self.frealign_dir, FREALIGN_INPUT_FILE_INFO_FILE)

    def read_frealign_input_file_info(self, frealign_input_info_file = None):
        '''
            read parameters from input file info file.
        '''
        if frealign_input_info_file is None:
            frealign_input_info_file = self.get_frealign_input_info_file()

        with open(frealign_input_info_file) as f_info_f:
            for line in f_info_f:
                if not line.strip(): continue
                tmp = line.strip().split()
                if len(tmp) < 2: continue
                stack_file = tmp[0]
                par_header = line.strip()[len(stack_file):].strip()
                stack_file = os.path.abspath(stack_file)
                fp = FrealignParameter()
                fp.par_file = par_header
                if not os.path.exists(stack_file):
                    raise Exception, "stack file %s does not exist" % stack_file
                self.stacks[stack_file] =  par_header
    
    def is_done(self, frealign_round):
        for stack_file, par in self.stacks.items():
            par_file = self.get_par_file(par.par_file, frealign_round)
            if not os.path.exists(par_file):
                return False
            
        return True
    
    def write_star(self, frealign_round):
        s_file = "%s_%d.star" % (self.info['output_header'], frealign_round)
        if os.path.exists(s_file):
            logger.warn("%s exists already, overwrite" % s_file)

        sum(self.stacks.values()).write(s_file)

    def read_particle_parameter_star(self, frealign_round):
        ''' Read frealign parameter from star file
        :param frealign_round: The frealign round
        :return: None if could not read, otherwise the {stack:param} dict
        '''
        from relion import Metadata
        s_file = "%s_%d.star" % (self.info['output_header'], frealign_round)
        if not os.path.exists(s_file):
            return None
        m = Metadata.readFile(s_file)
        # group by the stack_file
        m_stks = m.group_by("stack")
        for stack_file, fp in self.stacks.items():
            if not stack_file in m_stks:
                logger.wran("stack file %s not in star file %s" % (stack_file, s_file))
                return None
            if len(m_stks[stack_file]) != len(fp):
                logger.warn("The stack in existing star is not same as in original par for %s" % stack_file)
                return None
        return m_stks 


    def read_particle_parameter(self, frealign_round):
        '''Read Frealign parameter for round 
        @param frealign_round: the refinement round to read 
        '''
        if self.stacks is None:
            self.stacks = self.read_frealign_input_file_info()

        # read star file first
        if os.path.exists("%s_%d.star" % (self.info['output_header'], frealign_round)):
            r = self.read_particle_parameter_star(frealign_round)
            if r is not None:
                return r
        
        for stack_file, fp in self.stacks.items():
            if type(fp) is FrealignParameter:
                par_file = fp.par_file
            elif type(fp) is str:
                par_file = fp
            par_file = self.get_par_file(par_file, frealign_round)

            if not os.path.exists(par_file):
                raise FrealignNotCompleted("%s does not exists" % par_file)
            fp = FrealignParameter()
            fp.load_param_from_file(par_file, self.info['voxel'])
            self.stacks[stack_file] = fp
            # read box file info
            box_file, fmt = None, None
            box_dir = os.path.join(self.frealign_dir, "box_files")
            if os.path.exists(box_dir):
                box_file = os.path.join(box_dir, "%s.box" % re.sub("_\d+\.par", "", os.path.basename(par_file)))
                fmt = 'eman'
            else:
                box_dir = os.path.join(self.frealign_dir, 'vecs')
                if os.path.exists(box_dir):
                    box_file = glob.glob("%s/%s*" %  (box_dir, re.sub("_\d+\.par", "", os.path.basename(par_file))))[0]
                    fmt = 'lmbfgs_vec'
            if box_file is not None:
                from chuff.util.box import Box
                try:
                    boxes = Box.read_from_file(box_file, fmt)
                    for ii in range(len(fp)):
                        fp[ii].x = boxes[ii].center.x
                        fp[ii].y = boxes[ii].center.y
                except:
                    import sys
                    print sys.exc_info()
                    print "Error reading box file %s with format: %s" % (box_file, fmt)

        self.fealign_round = frealign_round    
        return self.stacks
    
    def write_frealign_input_file_info(self, frealign_input_info_file = None):
        '''
            write frealign input info file.
        '''
        if frealign_input_info_file is None:
            frealign_input_info_file = self.get_frealign_input_info_file()
        with open(frealign_input_info_file, 'w') as f_info_f:
            for stack_file in self.stacks:
                fp = self.stacks[stack_file]
                par_root = re.sub("_\d+\.par$", '', self.get_par_file(fp.par_file, 0))
                f_info_f.write("%s %s\n" % (stack_file, par_root))
    def get_dir(self, d):
        return os.path.join(self.frealign_dir, d)

    def write_box_files(self, box_dir = None):
        if box_dir is None:
            box_dir = self.get_dir('box_files')
        if not os.path.exists(box_dir):
            try:
                os.mkdir(box_dir)
            except:
                pass
        for fp in self.stacks.values():
            box_file = os.path.join(box_dir, "%s.box" % re.sub("_\d+\.par", "", os.path.basename(fp.par_file)))
            fp.write_box_file(box_file)
    
    def add_parameter(self, par):
        '''
            add one parameter file, with stack
        '''
        self.stacks[par.stack_file] = par
    

    def run_in_dir(self, new_frealign_dir, **kwargs):
        self.prepare_dir(new_frealign_dir)
        return Frealign.run_chuff(new_frealign_dir, **kwargs)

    def copy(self, frealign_dir = None, info_only = False):
        if frealign_dir is None:
            frealign_dir = self.frealign_dir
        fr = Frealign(self.frealign_dir)
        fr.info = deepcopy(self.info)
        fr.frealign_round = self.frealign_round
        if not info_only:
            fr.stacks = self.stacks
        return fr

 
    def __str__(self):
        rs = ''
        rs += 'info: %s\n' % self.info
        rs += 'nptcls: %s\n' % sum([len(stk) for stk in self.stacks.values()])
        if len(self.stacks) > 0:
            rs += 'frealign_dir: %s\n' % self.frealign_dir
        rs += "Round: %d\n" % self.frealign_round
        return rs
    
    def num_particles(self):
        return sum([len(stk) for stk in self.stacks.values()])

from relion import MPIParameter
from cryofilia.EMImage import EMImage
class MicrographID(Metadata):
    _labels={
            "mg_name" : ["MicrographName"],
            "tube_id"   : ["HelicalTubeID"],
            "mg_id"     : ["micrographID"]
            }

class InitFrealignParameter(MPIParameter):
    def add_arguments(self):
        self.add_argument('input_star', required = True, widget = "FileChooser",
                 help = "the input parameters", group="io")
        self.add_argument('box_size', type = int,
                help = "the box size used for refinement", group="ptcl")
        self.add_argument('bin_factor', type=int, required = True,
                help = "the bin factor", group="ptcl")
        self.add_argument('num_pfs', type = int,
                help = "number of protofilimant", group="ptcl")
        self.add_argument('num_starts', type = int, default = 3,
                help = "number of starts", group="ptcl")
        self.add_argument('actin', action = 'store_true', group="ptcl",
                help = "actin means regular filament")
        self.add_argument('recenter', action = 'store_true', group="ptcl",
                help = "recenter the particle to make shift to 0")
        self.add_argument('prerotate', action = 'store_true',
                help = "prerotate particle to make inplane rotation angle 0")
        self.add_argument('by_filament', action = 'store_true',
                help="group id by filament")
        self.add_argument('do_not_output_stack', action = 'store_true',
                help="don't output stack file")
        self.add_argument('rise', type=float, help="helical rise")
        self.add_argument('twist', type=float, help="helical twist")
        self.add_argument('cores', type=int, help="number of parallel procs")

        super(InitFrealignParameter, self).add_arguments()

# to add the labels from ref align
from cryofilia.helix.ref_align import RefAlignData
class InitFrealign(InitFrealignParameter):
    _label="CFAutoRefine" 
    def run(self):
        if self._rank == 0:
            if self.debug:
                logging.getLogger().setLevel(logging.DEBUG)
            self.workdir = self.workdir or self.get_next_workdir(label="CFAutoRefine")
            logging.info("Reading star file")
            m = FrealignParameter.readFile(self.input_star)
            m.renumber()
            # set all occs
            for m1 in m:
                m1.occ = 1.0
            ptcl_dir = self.path('ptcls')
            if not os.path.exists(ptcl_dir):
                self.mkdir(ptcl_dir)
            if self.box_size <= 0.:
                radius = self.cf.filament_outer_radius
                self.box_size = int(radius * 3 / self.cf.pixel_size) / self.bin_factor /  4 * 4
            else:
                self.box_size=int(self.box_size)

            m.stack_file=os.path.join(ptcl_dir, "frealign_stack.mrc")
            m.par_file="input_0.par"
        
            # check if relion pixel size is difference than the micrograph pixel size. 
            # this happened when relion extract particles with rescale the ptcl(downsampling)

            relion_pixel_size=m[0]['DetectorPixelSize'] * 10000. / m[0]['Magnification']
            scale=relion_pixel_size / self.cf.micrograph_pixel_size
            # if the pixel size is difference, recalculate the shift
            if abs(scale - 1.) > 0.001:
                for ptcl in m:
                    ptcl.shx*=scale
                    ptcl.shy*=scale
            # currently the shift is in original pixel size scale.
            # set the pixel size for frealign. 
            for ptcl in m:
                ptcl.pixel_size = self.cf.micrograph_pixel_size * self.bin_factor
            if self.rise == 0.0:
                logger.info("Calculating helical parameters:")
                self.rise, self.twist = calculate_helical_parameter_from_star_file(m)
            logger.info("rise: %.4f, twist: %.4f" % (self.rise, self.twist))

            target_magnification=self.cf.target_magnification / self.bin_factor
            # set the coordinate to integer, for boxing faster
            for ptcl in m:
                if self.recenter:
                    ptcl.recenter()
                cx,cy=ptcl.x, ptcl.y
                ptcl.x, ptcl.y=int(cx), int(cy)
                # frealign_par has different sign than relion
                ptcl.shx = ptcl.shx - cx + int(cx)
                ptcl.shy = ptcl.shy - cy + int(cy)
                ptcl.magnification=target_magnification
                ptcl['DetectorPixelSize'] = self.cf.scanner_pixel_size
                ptcl.stack = m.stack_file
            fr = Frealign(self.workdir)
            fr.info['output_header']=os.path.join(fr.frealign_dir, 'out')

            m_id=MicrographID()
            mgs = m.group_by("MicrographName")
            mg_id = 1
            for mg_key, mg in mgs.items():
                if self.by_filament:
                    filas = mg.group_by("HelicalTubeID")
                    for tube_id, mg in filas.items():
                        for ptcl in mg:
                            ptcl.mg_id =mg_id
                        mg_id += 1
                        m_id_1 = m_id.newObject()
                        m_id_1.mg_name=mg_key
                        m_id_1.tube_id=tube_id
                        m_id_1.mg_id=mg_id
                        m_id.append(m_id_1)
                else:
                    for ptcl in mg:
                        ptcl.mg_id = mg_id
                    mg_id += 1
                    m_id_1=m_id.newObject()
                    m_id_1.mg_name=mg_key
                    m_id_1.tube_id=0
                    m_id_1.mg_id=mg_id
                    m_id.append(m_id_1)
            m.write(self.path("input_renumbered.star"))
            if not self.actin and (self.num_pfs != 0 or m[0].hasKey('pfs')):
                fr.info['microtubule'] = 1
                if self.num_pfs == 0:
                    if m[0].hasKey('pfs'):
                        self.num_pfs = m[0]['pfs']
                fr.info['num_pfs'] = self.num_pfs
                if self.num_starts == 0:
                    if m[0].hasKey('starts'):
                        self.num_start=m[0]['starts']
                fr.info['num_starts']=self.num_starts
            
            fr.info['helical_twist'] = self.twist
            fr.info['helical_rise']=self.rise
            fr.info['micrograph_pixels_per_repeat'] = self.rise / self.cf.micrograph_pixel_size
            fr.info['voxel']=self.cf.micrograph_pixel_size * self.bin_factor
            fr.info['bin_factor']=self.bin_factor

            fr.add_parameter(m)
            m_id.write(self.path('mg_id.star'))
            
            if self._rank == 0:
                FrealignRunner(fr).prepare_dir()
            if self.do_not_output_stack:
                return
            # extract ptcls
            if not os.path.exists(m.stack_file):
                logger.debug("Allocating disk space")
                stk_em=EMImage(self.box_size, self.box_size, len(m), 
                        mmap=True, mmap_file=m.stack_file, mode='w+')
                del stk_em
            
            def g():
                start = 0
                for end in range(1, len(m)):
                    if end == len(m)-1 or m[end].mg_id != m[start].mg_id:
                        yield [m, self.box_size, m.stack_file, start, end, self.bin_factor, self.prerotate], {}
                        start = end
        else:
            g = object
        self.workdir = self.bcast(self.workdir)
        if self.do_not_output_stack:
            return
        if self._rank == 0:
            logger.info("Extracting stack file")
        self.parallel_job(self.extract, g(), progress_bar=True)
    
    def renumber_micrograph_id(self, mgs):
        pass

    def extract(self, fp, box_size, stk_file, start, end, bin_factor, prerotate):
        stk_em = EMImage(fp.stack_file, mmap=True, mode='r+')
        stk_arr = stk_em.data
        n = len(fp)
        if stk_arr.shape != (n, box_size, box_size):
            raise Exception("stack size is wrong: file: %s, fp: %s" % (stk_arr.shape, (n, box_size, box_size)))
        unbin_box_size = box_size * bin_factor
        # used to do rotation
        unbin_box_size_r = (1.5 * unbin_box_size +1) // 2 * 2
        stk = EMImage(box_size, box_size, n)
        for idx in range(start, end+1):
            ptcl=fp[idx]
            mgname = ptcl['MicrographName']
            mg = EMImage(mgname, mmap=True)
            center = ptcl.x, ptcl.y
            if prerotate:
                p = mg.box(center, [unbin_box_size_r, unbin_box_size_r])
                p.rotate2d(ptcl.psi)
                ptcl.psi = 0.
                p = p.clip(unbin_box_size)
            else:
                 p = mg.box(center, [unbin_box_size, unbin_box_size])
            
            if bin_factor != 1:
                p.resample(box_size)
            p.normalize()
            stk_arr[idx - 1, :, :] = p.data
            stk.data[idx-1-start, :, :] = p.data
        return start, end

class FrealignRunnerParameter(MPIParameter):
    ''' Should already extracted particles, and put the ptcl info in star file
        id@ptcl_stackname
    '''
    def add_arguments(self):
        self.add_argument('round', default=1, type=int,
                help="frealign round")
        self.add_argument('input_star', required=True,
                help="Input star file")
        super(FrealignRunnerParameter, self).add_arguments()

class FrealignRunner(FrealignRunnerParameter):
    ''' FrealignRunner is used to run frealign.
    '''
    def __init__(self, frealign = None):
        super(FrealignRunner, self).__init__()
        self.frealign=frealign

    def run(self):
        pass

    @staticmethod
    def run_chuff(frealign_dir, background = False, stdout = None, stderr = None, remote = None, name = None, **kwargs):
        '''
            calling chuff_frealign to run the frealign. Old way
            
            :param **kwargs: The parameter passed to chuff_frealign_parallel. 
            :param frealign_dir: The frealign_dir in chuff way
            :param name: The name for the job, display use only
        '''
        from chuff.util.shell import tcsh
        args = []
        args.append('input_file_info = %s/frealign_input_file_info.txt' % frealign_dir)
        for key, value in kwargs.items():
            args.append("%s=%s" % (key, value))
        if name is None:
            server_name = remote
            if server_name is None:
                import socket
                server_name = socket.gethostname()
            name = "chuff_frealign@%s" % server_name
        rc = tcsh('chuff_frealign', *args, stdout = stdout, background = True, remote = remote, stderr = stderr, name = name)
        return rc
    
    def align_parallel(self, input_vol,  **kwargs):
        '''
            align will general a new frealign instance that contains refined parameters.
        '''
        if not os.path.exists(input_vol):
            raise Exception, "input volume does not exists: %s" % input_vol

        logging.debug("ALIGNMENT PARALLEL STARTED")

        frs = self.run_parallel(frealign_mode = 1, input_vol = input_vol, write_vol = 0, **kwargs)
        fr = Frealign.combine(frs, frealign_dir = self.frealign_dir)
        # we will copy one set of input to the frealign dir
        fr.write_parameter_files()
        shutil.copy("%s_%d.in" % (frs[0].info['output_header'], frs[0].frealign_round ), "%s_%s_align.in" % (self.info['output_header'], self.frealign_round))

        #self.stacks = fr.stacks
        #self.set_frealign_dir(self.frealign_dir)
        logger.debug("ALIGNMENT PARALLEL FINISHED")

        for fr1 in frs:
            shutil.rmtree(fr1.frealign_dir)
        return fr

    def reconstruct_parallel(self, **kwargs):
        '''
            reconstruct will not generate new frealign instance, just generate the reconstruction and statistics
        '''
        logging.debug("RECONSTRUCT PARALLEL STARTED")
        merge3d_prog = kwargs.get('merge3d_prog', None)
        if "merge3d_prog" in kwargs:
            del kwargs['merge3d_prog']
        if 'microtubule' not in self.info:
            kwargs['microtubule'] = 0
            if 'helical_twist' in self.info:
                kwargs['frealign_twist'] = - float(self.info['helical_twist'])
        else:
            kwargs['post_process_mt'] = 0
        frs = self.run_parallel(frealign_mode = 0, write_vol = 1, fdump='T', imem = 1, **kwargs)
        logging.info("Merging")
        stdout = kwargs.get('stdout')
        stderr = kwargs.get('stderr')
#        time.sleep(120)
        Frealign.merge(frs, "%s_%d.spi" % (self.info['output_header'], self.frealign_round), stdout = stdout, stderr = stderr, merge3d_prog = merge3d_prog)
        try:
            shutil.copy("%s_%d.in" % (frs[0].info['output_header'], frs[0].frealign_round ), "%s_%s_recon.in" % (self.info['output_header'], self.frealign_round))
        except:
            logger.warning("Cound not copy frealign input file %s" % ("%s_%d.in" % (frs[0].info['output_header'], frs[0].frealign_round )))

        # remove tmp directories
    
#        for fr in frs:
#            shutil.rmtree(fr.frealign_dir)
        logger.info('RECONSTRUCT PARALLEL FINISHED')

    def run_parallel(self, hosts = None, ncpus = 2, scratch_dir = None, stdout = None, stderr = None, **kwargs):

        if hosts is None:
            import socket
            hosts = [socket.gethostname()]
        import tempfile
        if scratch_dir is None: scratch_dir = get_default_scratch_dir()
        frs = self.split(ncpus, scratch_dir = scratch_dir)
        for fr in frs:
            logger.debug(str(fr))
            break
        procs = {}
        frealign_round = self.frealign_round
        start_time = [0 for _ in range(ncpus)]
        for i in range(ncpus):
            f = frs[i]
            f.frealign_round = frealign_round
            f.prepare_dir()
            if i > 0:
                stdout1, stderr1 = None, None
            else:
                stdout1, stderr1 = stdout, stderr
            proc = Frealign.run_chuff(f.frealign_dir, name = "%s@%s#%d" % ("chuff_frealign", hosts[i % len(hosts)], i), remote = hosts[i % len(hosts)], stdout = stdout1, stderr = stderr1, round = round, **kwargs)
            start_time[i] = time.time()
            procs[i] = proc
        i = 1
        while True:
            all_finished = True
            for j, proc in procs.items():
                rs = proc.poll()
                if rs is None:
                    all_finished = False
                else:
                    logger.info("%d / %d on %s done" % (i, ncpus, proc.name))
                    nptcls = sum([len(stk.particles) for stk in frs[j].stacks])
                    if nptcls > 0:
                        logger.debug('total ptcls: %d' % (nptcls)) 
                        logger.debug('avg time per 100 ptcls: %.4fs' % (100.0 * (time.time() - start_time[j]) / nptcls)) 
                    del procs[j]
                    i += 1

            if not all_finished:
                time.sleep(10)
            else:
                break

        for f in frs:
            f.stacks = []
            f.read(frealign_round = frealign_round)
        return frs
    
    def reconstruct(self, host, **kwargs):
        proc = Frealign.run_chuff(self.frealign_dir, remote = host, write_vol = 1, frealign_mode = 0, frealign_round=self.frealign_round, **kwargs)
        return proc

    def align(self, input_vol, host = None, **kwargs):
        proc = Frealign.run_chuff(self.frealign_dir, remote = host, write_vol = 0, frealign_mode = 1, input_vol = input_vol, frealign_round = self.frealign_round, **kwargs)
        return proc
    @staticmethod
    def merge(*args, **kwargs):
        return Frealign.merge_volume(*args, **kwargs)
    @staticmethod
    def merge_volume(frs, output_volume, remote = None, stdout = None, stderr = None, merge3d_prog = None):
        '''
            This is used to merge several volumes generated by fdump='T'
        '''
        if merge3d_prog is None:
            merge3d_prog = merge3d
        import tempfile
        d = tempfile.mkdtemp()
        merge_in = os.path.join(d, 'merge_3d.in')
        f = open(merge_in, 'w')
        N = 0

        for fr in frs:
            if len(fr.stacks) > 0:
                N += 1
        f.write("%d\n" % N)
        for fr in frs:
            if len(fr.stacks) == 0: continue
            f.write("%s_%d.spi\n" % (fr.info['output_header'], fr.round))
        bname = os.path.splitext(output_volume)[0]
        for ext in ['.res', '.spi', '.wgt', '_fsc1.spi', '_fsc2.spi', '_phasediffs', '_pointspread']:
            f.write("%s%s\n" % (bname, ext))
        f.close()
        f_merge_in = open(merge_in)
        from chuff.util.shell import tcsh
        rc = tcsh(merge3d_prog, remote = remote, stdin = f_merge_in, stdout = stdout, stderr = stderr)
        logger.info("merge3d return %d" % rc)
        
        import shutil
        shutil.rmtree(d)
    def prepare_dir(self, new_frealign_dir = None, frealign_round = None):
        '''
            prepare a frealign run dir for running frealign
            write info_file
            write frealign_input_file_info file
            write box files
        '''
        if frealign_round is None:
            frealign_round = 1
        self.frealign.frealign_round = frealign_round
        if new_frealign_dir is None:
            new_frealign_dir = self.frealign.frealign_dir
        if not os.path.exists(new_frealign_dir):
            try:
                os.makedirs(new_frealign_dir)
            except:
                pass
        if not os.path.exists(os.path.join(new_frealign_dir, 'filament_data')):
            try:
                os.mkdir(os.path.join(new_frealign_dir, 'filament_data'))
            except:
                pass
        self.frealign.set_frealign_dir(new_frealign_dir)
        # save info file
        
        self.frealign.write_info_file()
        # save frealign_input_file_info
        self.frealign.write_frealign_input_file_info()
        
        # write par files
        self.frealign.write_parameter_files(frealign_round = frealign_round - 1)
        # write box files
        self.frealign.write_box_files()

def get_default_scratch_dir():
    return SCRATCH_DIR

def calculate_helical_parameter_from_star_file(m, rise = None, twist = None):
    '''
    '''

    from cryofilia.helix.smooth_function import fix_rollover
    logger.info("Calculating helical parameter...")
    import math, numpy as np
    mts = m.group_by('MicrographName', 'HelicalTubeID')
    all_dists = []
    all_twists = []
    pixel_size = m[0]['rlnDetectorPixelSize'] * 10000 / m[0]['rlnMagnification']
    for mt in mts.values():
        if len(mt) <= 5: continue
        xs, ys, shxs, shys, phis, psis = mt.getValues(['CoordinateX', 'CoordinateY', 'OriginX', 'OriginY', 'AngleRot', 'AnglePsi'])
        phis,_ = fix_rollover(phis, 0)
        twist = np.polyfit(range(len(phis)), phis, 1)[0]
        all_twists.append(twist)
        cs = np.array(zip(xs, ys))
        dists = np.linalg.norm(cs[1:, :] - cs[:-1, :], axis=1)
        all_dists.extend(dists)
    from scipy.stats import norm
    n, bins = np.histogram(all_twists, bins="auto")
    idx = np.argmax(n)
    avg = bins[idx]
    a = np.array(all_twists)
    a = a[abs(a-bins[idx]) < 10]
    avg_twist, _ =  norm.fit(a)
    n, bins = np.histogram(all_dists, bins="auto")
    idx = np.argmax(n)
    avg = bins[idx]
    avg=np.median(all_dists)
    a = np.array(all_dists)
    a = a[abs(a-avg) < 3]
    avg_dist, _ =  norm.fit(a)
    helical_rise = avg_dist * pixel_size
    helical_twist = avg_twist
    # check if radonRepeatDistanceFilament exists

    #if m[0].hasKey("radonRepeatDistanceFilament"):
    #    dists = m.getValues('radonRepeatDistanceFilament')
    #    helical_rise =sum(dists) / len(dists)
    #    print helical_rise
    #else:
    #    logger.debug("No radonRepeatDistanceFilament exists")
    #    logger.debug("%s" % m.getActiveLabels())
    return helical_rise, helical_twist


