""" Unittest for a single command
"""
import unittest
import tempfile
import os
import shutil
from testCF import TestWithFile

skipped_test=[]
skipped_test.append("cf_smooth_star")

chuff_dir = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "..", ".."))
# chuff_dir = "/home/xueqi/Documents/py_cf_mt"
class TestCommand(TestWithFile):
    def setUp(self):
        super(TestCommand, self).setUp()
        self.wd = tempfile.mkdtemp(dir="/tmp")
    def tearDown(self):
        super(TestCommand, self).tearDown()
        import shutil
        if self.wd.startswith("/tmp"):
            shutil.rmtree(self.wd)
            
    def command(self, cmd, env=None, input="", input_dir=None):
        import subprocess as sp
        os.chdir(self.wd)
        script = "random_file.script"
        open(script, 'w').write('''#!/bin/bash
source "{chuff_dir}/chuff.bashrc"
{cmd}'''.format(chuff_dir=chuff_dir, cmd=cmd))
        os.chmod(script, 0700)
        if input_dir is not None:
            import glob
            for f in glob.glob("%s/*" % input_dir):
                if os.path.isfile(f):
                    shutil.copy(f,".")
                elif os.path.isdir(f):
                    shutil.copytree(f,os.path.basename(f))
                
        p = sp.Popen(["./{}".format(script)], stdout=sp.PIPE, stderr=sp.PIPE,
                     stdin=sp.PIPE)
        stdout, stderr = p.communicate(input)
        self.assertEqual("", stderr, msg=stderr)
    
    def _test_command(self):
        import glob
        os.chdir(self.temp_dir)
        for d_dir in os.listdir(self.data_dir):
            test_name = d_dir
            os.mkdir(test_name)
            os.chdir(test_name)
            input_srcs = os.path.join(self.data_dir, d_dir, "input")
            for f in glob.glob("%s/*" % input_srcs):
                if os.path.isfile(f):
                    shutil.copy(f,".")
                elif os.path.isdir(f):
                    shutil.copytree(f,os.path.basename(f))
                
            rtn = os.system('chuff_dir="%s" ./cmd.sh' % chuff_dir)
            self.assertEqual(0, rtn, open('cmd.sh').read())
            os.chdir(self.temp_dir)
            