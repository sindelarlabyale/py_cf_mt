'''
Created on Jun 19, 2017

@author: xueqi
'''
import unittest
from cryofilia.proj import EMProjector


class TestProjection(unittest.TestCase):

    @unittest.skip("tmp")
    def testProject(self):
        from cryofilia.EMImage import EMImage
        p = EMProjector('mt_pf14.mrc', interp_mode='nearest')
        m = p._project(10,90,12,0,0)
        m.write_image('a.mrc')
        
    @unittest.skip("tmp")
    def testProfiling(self):
        from cryofilia.EMImage import EMImage
        p = EMProjector('out_6.mrc')
        import cProfile, pstats, StringIO
        from random import random as rr
        #m = p._project(360 * rr(),360 * rr(),360 * rr(),0,0)

        pr = cProfile.Profile()
        pr.enable()
        for i in range(1000):
            m = p._project( 360 * rr(),360 * rr(),360 * rr(),0,0)
        pr.disable()
        s=StringIO.StringIO()
        sortby = "cumulative"
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print "\n".join(s.getvalue().split("\n")[:20])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()