'''
Created on May 26, 2017

@author: xueqi
'''
import unittest

from cryofilia.EMImage import EMImage
import numpy as np

class TestEMImage(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass

    
    def testInit(self):
        import numpy as np
        # init an zero filled one dim image
        img = EMImage(4)
        self.assertEqual(img.get_xsize(), 4)
        self.assertEqual(img.ndim, 1)
        self.assertEqual(img.data.tolist(), [0.] * 4)
        # reassign the data
        img.data = np.zeros([4,4])
        self.assertEqual(img.ndim, 2)
        
    def testPaste(self):
        img = EMImage(4)
        img1 = EMImage(2)
        img1.add(1)
        img.paste(img1)
        self.assertEqual(img.data.tolist(), [0., 1., 1., 0.])
        
        img = EMImage(4)
        img.paste(img1, center1 = 0)
        self.assertEqual(img.data.tolist(), [1., 0., 0., 0.])
        img = EMImage(4)
        img.paste(img1, center2 = 0)
        self.assertEqual(img.data.tolist(), [0., 0., 1., 1.])
    
    def testResample(self):
        ''' Resample image
        '''
        import numpy as np
        img = EMImage(4)
        img.data = np.arange(4)
        img.resample(2)
        self.assertEqual(img.data.tolist(), [2., 4.])
        
        a = np.array([[1, 2, 3, 4], 
                             [5, 6, 7, 8], 
                             [9, 0, 1, 2], 
                             [3, 4, 5, 6]])
        img.data = a
        img.resample(2)
        b = img.data
        img.data = a
        img.resample(2, useEMAN2 = True)
        c = img.data
        np.testing.assert_equal(b,c)
    
    def testClip(self):
        img = EMImage(1)
        a = np.array([[1, 2, 3, 4],
                             [5, 6, 7, 8], 
                             [9, 0, 1, 2], 
                             [3, 4, 5, 6]])
        a1 = np.array([[0, 0, 0, 0], 
                             [0, 6, 7, 0], 
                             [0, 0, 1, 0], 
                             [0, 0, 0, 0]])
        img.data = a
        b = img.clip(2)
        np.testing.assert_equal(b.data, np.array([[6,7], [0,1]]))
        b = img.clip(2, center = [3,3])
        np.testing.assert_equal(b.data, np.array([[1,2], [5,6]]))
        b = img.clip(3, center = [3,3])
        np.testing.assert_equal(b.data, np.array([[1,2, 0], [5,6, 0], [0, 0, 0]]))
        
        # padding
        img.data = np.array([[6,7], [0,1]])
        c = img.clip(4)
        np.testing.assert_equal(c.data, a1)
        # pad to odd
        img.data = np.array([[6,7], [0,1]])
        c = img.clip(3)
        np.testing.assert_equal(c.data, np.array([[6,7,0], [0, 1, 0], [0, 0, 0]]))
    
    def testGetOverlap(self):

        get_overlap = EMImage.get_overlap
        
        # no box size used, return all overalp
        bx, by = get_overlap(10, 5, 4, 2)
        self.assertSequenceEqual([bx,by],[[3,7],[0,4]])    
        # with exact size of one    
        bx, by = get_overlap(10, 5, 4, 2,4)
        self.assertSequenceEqual([bx,by],[[3,7],[0,4]])
        # with smaller boxsize
        bx, by = get_overlap(10, 5, 4, 2, 2)
        self.assertSequenceEqual([bx,by],[[4,6],[1,3]])
        # out of boundary in one shape
        bx, by = get_overlap(10, 5, 4, 0, 2)
        self.assertSequenceEqual([bx,by],[[5,6],[0,1]])       
        
        # odd dimension
        bx, by = get_overlap(10, 5, 4, 2, 3)
        self.assertSequenceEqual([bx,by],[[4,7],[1,4]])    
        
        # odd original dimension
        bx, by = get_overlap(9, 4, 4, 2, 4)
        self.assertSequenceEqual([bx,by],[[2,6],[0,4]])    
        bx, by = get_overlap(9, 4, 3, 1, 4)
        self.assertSequenceEqual([bx,by],[[3,6],[0,3]])  
        
        # a
        bx,by = get_overlap(11, 5, 84, 42)
        self.assertSequenceEqual([bx,by], [[0,11],[37,48]])
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()