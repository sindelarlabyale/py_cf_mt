'''
Created on Oct 30, 2017

@author: xueqi
'''
import unittest
from cryofilia.align import Aligner, AlignResultData, AlignResult
from testCryofilia import TestWithFile
from cryofilia.EMImage import EMImage
import warnings
from cryofilia.testImage import TestImage
warnings.simplefilter("error")


class TestAlignResesult(unittest.TestCase):

    def testInit(self):
        ard = AlignResultData()
        values = [1.1, 2, 3.3, 4, 5, 0.1]
        ard.shx, ard.shy, ard.phi, ard.theta, ard.psi, ard.ccc = values

class TestAligner(TestWithFile):
    
    def setUp(self):
        TestWithFile.setUp(self)
        
    def testTest(self):
        self.get_data_path("input.star")
        
    def testInit(self):
        aln = Aligner(None)
    
    def test_ccc(self):
        aln = Aligner(None)
        img = TestImage.circle(8, 3)
        img.shift_i(1, 1)
        # This test if no projector, raise Error
        self.assertRaises(RuntimeError, aln.ccc, img, 0, 0, 0)
        from cryofilia.proj import EMProjector
        proj = EMProjector(self.readImage("ref.mrc"))
        aln = Aligner(proj)
        #aln.debug = True
        r = aln.ccc(img, 0, 0, 0)
        self.assertSequenceAlmostEqual([-1, -1], [r.shx, r.shy])
        
    #@unittest.skip("Tmp")
    def test_ccc_proj(self):
        aln = Aligner(None)
        #img = self.readImage("img1.mrc")
        #ref = self.readImage("ref1.mrc")
        img = EMImage(4,4)
        x_fit, y_fit, ccc = aln.ccc_proj(img, img)
        self.assertEqual(1., ccc, "Self align pf constant should be 1")
        self.assertSequenceEqual([0., 0.], [x_fit, y_fit])
        ref = TestImage.radius([4,4])
        x_fit, y_fit, ccc = aln.ccc_proj(ref, ref)
        self.assertEqual(1., ccc, "Self align should be 1")
        self.assertSequenceAlmostEqual([0., 0.], [x_fit, y_fit])
        
        x_fit, y_fit, ccc = aln.ccc_proj(img, ref)
        self.assertEqual(-1., ccc, "Self align with only one constant should be 11")
        self.assertSequenceEqual([0., 0.], [x_fit, y_fit])
        
        
    def readImage(self, img_name):
        from cryofilia.EMImage import EMImage
        img = EMImage(self.get_data_path(img_name))
        return img
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()